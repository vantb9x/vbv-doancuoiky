 
 
 
    app.controller('MainSanPhamTheoLoaiController', 
  ['$scope',   '$mdDialog', '$mdMedia',   '$firebaseArray', '$routeParams', '$filter', 
  function($scope,  $mdDialog, $mdMedia, $firebaseArray, $routeParams, $filter)
{
  var refSanPhamTheoLoai = new Firebase("https://bacphu.firebaseio.com/SanPham/");
  
   
  $scope.ListSanPhamTheoLoai = [];
  

   var steppageTheoLoai = 0;
     var pageTheoLoai = 0;
  $scope.PageArrayTheoLoai = [];

  
   
    
  $scope.NumberProductOnPageTheoLoai = 6;
  $scope.currentPageTheoLoai = 0;

   $scope.beginTheoLoai = 0;
    $scope.endTheoLoai = $scope.NumberProductOnPageTheoLoai ;
   // var refSanPhamonPage = new Firebase("https://bacphu.firebaseio.com/SanPham/");
 
 $scope.SanPhamOnPageTheoLoai =  [];
 
  refSanPhamTheoLoai.orderByChild("LoaiSanPham").equalTo($routeParams.loaiSanPham).on("child_added", function(snapshot)
    {    
        $scope.ListSanPhamTheoLoai.push(snapshot.val());
        var bb = steppageTheoLoai;
     if (steppageTheoLoai === 0)
      {
        $scope.PageArrayTheoLoai.push(pageTheoLoai);
        pageTheoLoai++;
      }
       if (steppageTheoLoai === $scope.NumberProductOnPageTheoLoai - 1)
      {
        steppageTheoLoai = -1;
      }
      steppageTheoLoai++;

     
    });
  $scope.TimSanPhamTheoLoai = function(sanpham)
  {
    $scope.currentSanPhamTheoLoai = sanpham;
    $scope.currentSanPhamTheoLoai.currentShowImage = sanpham.ImagePhu1;
  }

$scope.changeShowImageTheoLoai = function(image)
{
  
   $scope.currentSanPhamTheoLoai.currentShowImage = image;
}


 $scope.TimIDXemPhongToHinhAnhTheoLoai = function(sanpham)
   {
    $scope.ImageSP = sanpham;
   }
   $scope.showBigPictureTheoLoai = function(ev)
   {
     var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;

       $mdDialog.show(
        {
              clickOutsideToClose: true,
              controller: DialogBigImageController,
              templateUrl: 'template/MainTemplatePhongToSanPham.html',
              parent: angular.element(document.body),
              targetEvent: ev,
              scope: $scope,
          preserveScope: true,
              fullscreen: useFullScreen
            })
          .then(function(answer) 
          {
            
          }, function()
          {
             $scope.status = 'You cancelled the dialog.';
             //hien dialog xem chi tiet ssan pham lai
             $scope.openDialogXemChiTietSanPhamTheoLoai();
              
          });


      $scope.$watch(function() 
      {
         return $mdMedia('xs') || $mdMedia('sm');
      }, function(wantsFullScreen) 
      {
          $scope.customFullscreen = (wantsFullScreen === true);
      });
   }

  
  //load san pham luc moi hien o che do mac dinh
   refSanPhamTheoLoai.orderByChild("LoaiSanPham").equalTo($routeParams.loaiSanPham).limitToFirst($scope.NumberProductOnPageTheoLoai).on("child_added", function(snapshot)
      {
          $scope.SanPhamOnPageTheoLoai.push(snapshot.val());
      });

  $scope.changeCurrentPageTheoLoai = function(it)
  {
    
    $scope.currentPageTheoLoai = it;
   
    uploadCurrentPageTheoLoai();
  }
  function uploadCurrentPageTheoLoai()
  {
    $scope.beginTheoLoai = $scope.currentPageTheoLoai * $scope.NumberProductOnPageTheoLoai ;
    $scope.endTheoLoai = $scope.currentPageTheoLoai * $scope.NumberProductOnPageTheoLoai  + $scope.NumberProductOnPageTheoLoai ;
    if($scope.selectedsapxepTheoLoai  === 1 || $scope.selectedsapxepTheoLoai === 2)
    { 
      $scope.SanPhamOnPageTheoLoai = $scope.SanPhamSapXepTheoLoai.slice($scope.beginTheoLoai , $scope.endTheoLoai);
    }
    else
    {
       $scope.SanPhamOnPageTheoLoai = $scope.ListSanPhamTheoLoai.slice($scope.beginTheoLoai , $scope.endTheoLoai);
    }
  }
  $scope.changePagePrevTheoLoai = function()
  {
    if ($scope.currentPageTheoLoai > 0)
    {
      $scope.currentPageTheoLoai--;
       uploadCurrentPageTheoLoai();
    }
  }
  $scope.changePageNextTheoLoai = function()
  {
    if ($scope.currentPageTheoLoai < $scope.PageArrayTheoLoai.length - 1)
    {
      $scope.currentPageTheoLoai++;
       uploadCurrentPageTheoLoai();
    }
   
  }
 

$scope.selectedsapxepTheoLoai = "0";
$scope.listsapxepTheoLoai = [{id:0, name: "Mặc định"},{id:1, name: "Giá tăng dần"},
{id: 2, name: "Giá giảm dần"}];    
  $scope.changesapxepTheoLoai = function(loaisapxepTheoLoai)
  {
       $scope.currentPageTheoLoai = 0;
     $scope.SanPhamSapXepTheoLoai = [];
      $scope.selectedsapxepTheoLoai = loaisapxepTheoLoai.id;
      if ( $scope.selectedsapxepTheoLoai === 1)
      {
          $scope.SanPhamSapXepTheoLoai = $filter('orderBy')($scope.ListSanPhamTheoLoai, 'GiaSanPham', false);
           uploadCurrentPageTheoLoai();
            
      }
      else if ( $scope.selectedsapxepTheoLoai === 2)
      {
          $scope.SanPhamSapXepTheoLoai = $filter('orderBy')($scope.ListSanPhamTheoLoai, 'GiaSanPham', true);
           uploadCurrentPageTheoLoai();
         
      }
      else
      {
          
        $scope.SanPhamSapXepTheoLoai =  $filter('orderBy')($scope.ListSanPhamTheoLoai, 'ID', false);
        uploadCurrentPageTheoLoai();
      }
     
  }  

 // $scope.$watch('SearchSanPham', function(val)
 //    { 

 //        $scope.SanPhamSapXepTheoLoai = $filter('filter')($scope.ListSanPhamTheoLoai, $scope.SearchSanPham);
 //           uploadCurrentPageTheoLoai();
 //           alert($scope.SanPhamSapXepTheoLoai.length);
 
 //    });
  
    //xem chi tiet san pham
    $scope.openDialogXemChiTietSanPhamTheoLoai = function(ev)
    {

      var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;

        $mdDialog.show(
        {
            clickOutsideToClose: true,
            controller: DialogControllerTheoLoai,
            templateUrl: 'template/MainTemplateXemCTSPTheoLoai.html',
            scope: $scope,
           preserveScope: true,
            parent: angular.element(document.body),
            targetEvent: ev,
            fullscreen: useFullScreen
        })
          .then(function(answer) {
          $scope.status = 'You said the information was "' + answer + '".';
        }, function() {
         $scope.status = 'You cancelled the dialog.';
        });


        $scope.$watch(function() {
          return $mdMedia('xs') || $mdMedia('sm');
         }, function(wantsFullScreen) {
         $scope.customFullscreen = (wantsFullScreen === true);
        });
    }
 
 }]);
 

 function DialogControllerTheoLoai($scope, $mdDialog) {
  $scope.hide = function() {
    $mdDialog.hide();
  };

  $scope.cancel = function() {
    $mdDialog.cancel();
  };

  $scope.answer = function(answer) {
    $mdDialog.hide(answer);
  };

  $scope.boVaoGioHang11TheoLoai = function(item)
  {
     var index;
  //  alert($scope.gioHang.length);

  //item khong xuat hien trong gio hang
    if (KiemTraPhanTuTonTaiTheoID($scope.gioHang, item) === -1)
      {
         
        item.SoLuongMua = $scope.SoLuongSanPham;
        $scope.gioHang.push(item);


         item.ThanhTien = 
       (parseInt(ChuyenChuoiGiaMatSpace(item.GiaSanPham))
       * item.SoLuongMua);
       item.ThanhTienSpace = ChuyenGiaSangStringSpace(item.ThanhTien);
       
        $scope.TongThanhToan += (parseInt(ChuyenChuoiGiaMatSpace(item.GiaSanPham)) * item.SoLuongMua);
      $scope.TongThanhToanSpace = ChuyenGiaSangStringSpace($scope.TongThanhToan);

       $scope.SoLuongSanPhamGioHangDem++;
      $mdDialog.hide();

      }
      else
      {
        
         index = KiemTraPhanTuTonTaiTheoID($scope.gioHang, item);
        $scope.gioHang[index].SoLuongMua = $scope.gioHang[index].SoLuongMua + $scope.SoLuongSanPham;

         $scope.gioHang[index].ThanhTien = 
       (parseInt(ChuyenChuoiGiaMatSpace(item.GiaSanPham))
       * $scope.gioHang[index].SoLuongMua);
        $scope.gioHang[index].ThanhTienSpace = ChuyenGiaSangStringSpace($scope.gioHang[index].ThanhTien);
       
        $scope.TongThanhToan += (parseInt(ChuyenChuoiGiaMatSpace(item.GiaSanPham))
          * $scope.gioHang[index].SoLuongMua);
      $scope.TongThanhToanSpace = ChuyenGiaSangStringSpace($scope.TongThanhToan);
      }
      // co can bam nhieu lan nut them vao gio hang so luong tang???
              
      
      $mdDialog.hide();
    
    //gan bien tra ve cho viec them san pham lan thu 2
      $scope.SoLuongSanPham = 1;
      
  }

   $scope.MuaNgaySanPhamChiTietTheoLoai = function(item)
    {

     if (KiemTraPhanTuTonTaiTheoID($scope.gioHang, item) === -1)
      {
         item.SoLuongMua = $scope.SoLuongSanPham;
        $scope.gioHang.push(item);

      //tinh tong tien thanh toan
        item.ThanhTien = 
       (parseInt(ChuyenChuoiGiaMatSpace(item.GiaSanPham))
       * item.SoLuongMua);
       item.ThanhTienSpace = ChuyenGiaSangStringSpace(item.ThanhTien);
       
        $scope.TongThanhToan += (parseInt(ChuyenChuoiGiaMatSpace(item.GiaSanPham)) * item.SoLuongMua);
        $scope.TongThanhToanSpace = ChuyenGiaSangStringSpace($scope.TongThanhToan);
     
     $scope.SoLuongSanPhamGioHangDem++;
    //gan bien tra ve cho viec them san pham lan thu 2
      $scope.SoLuongSanPham = 1;

    }
      $mdDialog.hide();
    //chuyen den bang gio hang thanh toan
      var url = window.location.href.split("#")[0] + "#/giohang";
     window.location.href = url;  
    }
    

 
}


app.controller('ChiTietLanNhapSanPhamController',function($scope, $firebaseArray, $mdDialog, $mdMedia)
{
	var refChiTietLanNhap = new Firebase("https://bacphu.firebaseio.com/ChiTietLanThemSanPham");
	
	var IDSanPham = $scope.CurrentSanPhamEdit.ID;

	$scope.ListChiTietLanNhapTheoID = [];

	refChiTietLanNhap.orderByChild("IDSanPham").equalTo(IDSanPham).on("child_added", function(snapshot)
		{
			$scope.ListChiTietLanNhapTheoID.push(snapshot.val());
			 console.log(snapshot.val());
		});	
});


app.controller('ChiTietLanNhapSanPhamTheoLoaiController',function($scope, $firebaseArray, $mdDialog, $mdMedia)
{
	var refChiTietLanNhapTheoLoai = new Firebase("https://bacphu.firebaseio.com/ChiTietLanThemSanPham");
	
	var IDSanPhamTheoLoai = $scope.CurrentSanPhamEditTheoLoai.ID;

	$scope.ListChiTietLanNhapTheoIDTheoLoai = [];

	refChiTietLanNhapTheoLoai.orderByChild("IDSanPham").equalTo(IDSanPhamTheoLoai).on("child_added", function(snapshot)
		{
			$scope.ListChiTietLanNhapTheoIDTheoLoai.push(snapshot.val());
			 console.log(snapshot.val());
		});	
});
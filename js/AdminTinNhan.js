app.controller('AdminTemplateTinNhanController', function($scope, $firebaseArray)
{
	 var refTinNhan = new Firebase("https://bacphu.firebaseio.com/TinNhan");

	 $scope.ListTinNhan = $firebaseArray(refTinNhan);

	 $scope.AdminPhanHoiTinNhanUpdate = function(item)
	 {
	 	if (KiemTraPhanTuTonTaiTheoID($scope.ListTinNhan, item) != -1)
	 	{
	 		var index = KiemTraPhanTuTonTaiTheoID($scope.ListTinNhan, item);
	 		var refKey = $scope.ListTinNhan[index].$id;
  
			var refItem = new Firebase("https://bacphu.firebaseio.com/TinNhan/"+ refKey);
  		 		 
		 	refItem.update({
			  "TinhTrang": true
			});
		}
	 }
});

function KiemTraPhanTuTonTaiTheoID(array, item)
{
  var length = array.length;
  
  for (var i = 0; i < length; i++)
  {
    if (array[i].ID === item.ID)
    {
      return i;
    }
  }
  return -1;
}


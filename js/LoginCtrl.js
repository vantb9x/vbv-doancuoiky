      
     var app = angular.module("myapp11", ["ngMaterial", "firebase"]);
 
 
app.controller('AppCtrl11', ["$scope", "$firebaseAuth", "authFactory",  function($scope, $firebaseAuth, authFactory)
 {
      var ref = new Firebase("https://bacphu.firebaseio.com/");
    auth = $firebaseAuth(ref);
 
     $scope.AdminLogin = function() {

        var user = $scope.username + "@gmail.com";
           var password = $scope.userpassword;
       
   var result = authFactory.authUser(user, password);
      result.then(function(authData){
        if (authData.uid === "2af3cf13-bdc6-41ee-b05c-76a14c743a2a")
        {
         console.log("User Successfully logged in with uid: ", authData.uid)
          var url = window.location.href;
            var length = url.length;
             var urlorigin = window.location.href.substring(0, length - 10);
              var adminUrl = urlorigin + "admin.html#/sanpham";
               
              window.location.href = adminUrl;  
            }
      }, function(error) {
        console.log("Authentication Failed: ", error)
      })
    };
 
   
}]);
                
    
    app.factory('authFactory', ['$firebaseAuth', function( $firebaseAuth) {
    var authFactory = {};
    var ref = new Firebase("https://bacphu.firebaseio.com/");
    
    var auth = $firebaseAuth(ref);
  

     // Authentication 
     authFactory.authUser = function(email, password) {
      return auth.$authWithPassword({
            email: email,
            password: password
        });
     }
    return authFactory;
}]);


  

        app.config(function($mdThemingProvider) {
        $mdThemingProvider.theme('default')
        .primaryPalette('pink', {
          'default': '400', // by default use shade 400 from the pink palette for primary intentions
          'hue-1': '100', // use shade 100 for the <code>md-hue-1</code> class
          'hue-2': '600', // use shade 600 for the <code>md-hue-2</code> class
          'hue-3': 'A100' // use shade A100 for the <code>md-hue-3</code> class
      })
        // If you specify less than all of the keys, it will inherit from the
        // default shades
        .accentPalette('grey', {
          'default': '700' // use shade 200 for default, and keep all other shades the same
      });
    });

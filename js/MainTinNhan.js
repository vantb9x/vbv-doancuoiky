function MainThemTinNhanController($scope, $mdDialog, $firebaseArray, $mdToast) {
  $scope.hide = function() {
    $mdDialog.hide();
  };

  $scope.cancel = function() {
    $mdDialog.cancel();
  };

  $scope.answer = function(answer) {
    $mdDialog.hide(answer);
  };

  $scope.ThemTinNhan = function()
  {

  	var Name = $scope.Name;
  	var Content = $scope.Content;
  	var Phone = $scope.Phone;
  	 var email = $scope.Email;

    var currentDate = new Date()
    var day = currentDate.getDate();
    var month = currentDate.getMonth() + 1;
    var year = currentDate.getFullYear();
    var curDate = day + "/" + month + "/" + year;
    
    var refTinNhan = new Firebase("https://bacphu.firebaseio.com/TinNhan");
 
   $scope.ListTinNhan = $firebaseArray(refTinNhan);
         
    $scope.ListTinNhan.$loaded().then(function(ListTinNhan)  
      {
        var lengthListTinNhan = ListTinNhan.length;
        
        var newIDTinNhan;
        if (lengthListTinNhan === 0)
        {
          newIDTinNhan = 0;
        }
        else 
        {
          newIDTinNhan = parseInt(ListTinNhan[lengthListTinNhan - 1].ID) + 1;
        }
         var newTinNhan = refTinNhan.push();
        newTinNhan.set(
        {
          ID: newIDTinNhan,
          NguoiGui : Name,
          NoiDung : Content,
          DienThoai: Phone,
          Email: email,
          NgayGui: curDate,
          TinhTrang: false
        },function(error) {
        if (error) {
            $mdToast.show(
         $mdToast.simple().textContent('Tin nhan cua ban chua duoc gui')
         .position('top right')
         .hideDelay(5000)

        );
        } else {
        $mdToast.show(
         $mdToast.simple().textContent('Cam on ban da de lai loi nhan cho chung toi')
         .position('top right')
         .hideDelay(5000)

        );
         }
        });

      });
  	
  	
  	$mdDialog.cancel();
  }
}






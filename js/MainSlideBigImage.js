function DialogBigImageController($scope, $mdDialog) {
  $scope.hide = function() {
    $mdDialog.hide();
  };

  $scope.cancel = function() {
    $mdDialog.cancel();
  };

  $scope.answer = function(answer) {
    $mdDialog.hide(answer);
  };

     //slide big image
     $scope.slides = [
      {image: $scope.ImageSP.ImagePhu1 , description: 'Image 00'},
      {image: $scope.ImageSP.ImagePhu2, description: 'Image 01'},
   {image: $scope.ImageSP.ImagePhu3, description: 'Image 02'} 
    //    {image: $scope.ImageSP.ImagePhu3, description: 'Image 03'}
                  
        ];
       $scope.direction = 'left';
        $scope.currentIndex = 0;

        $scope.setCurrentSlideIndex = function (index) 
        {
            
            if (index > $scope.currentIndex)
            {
                 $scope.direction = 'left';
            }  
            else
            {
                 $scope.direction = 'right';
            }
            $scope.currentIndex = index;
        };

        $scope.isCurrentSlideIndex = function ($index) {
            //alert("index:" + $index + ", cur: " + $scope.currentIndex);

            if ($scope.currentIndex === $index)
                return true;
            return false;
        };

        $scope.prevSlide = function () {
           
            $scope.direction = 'left';
           
            if ($scope.currentIndex === $scope.slides.length -1)
            {
                 $scope.currentIndex = 0;
            } 
            else
            {
                $scope.currentIndex = $scope.currentIndex + 1;
            }   
             
        };

        $scope.nextSlide = function () {
           
             $scope.direction = 'right';
          
            if ($scope.currentIndex === 0) 
            {
                  $scope.currentIndex = $scope.slides.length - 1;
            }
            else
            {
               $scope.currentIndex = $scope.currentIndex - 1;
            }  
              

        };
}
		
app.animation('.slide-animation', function () {
        return {
            beforeAddClass: function (element, className, done) {
                var scope = element.scope();

                if (className == 'ng-hide') {
                    var finishPoint = element.parent().width();
                    if(scope.direction !== 'right') {
                        finishPoint = -finishPoint;
                    }
                    TweenMax.to(element, 0.5, {left: finishPoint, onComplete: done });
                }
                else {
                    done();
                }
            },
            removeClass: function (element, className, done) {
                var scope = element.scope();

                if (className == 'ng-hide') {
                    element.removeClass('ng-hide');

                    var startPoint = element.parent().width();
                    if(scope.direction === 'right') {
                        startPoint = -startPoint;
                    }

                    TweenMax.fromTo(element, 0.5, { left: startPoint }, {left: 0, onComplete: done });
                }
                else {
                    done();
                }
            }
        };
    });

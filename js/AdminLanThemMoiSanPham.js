

app.controller('AdminLanThemMoiSanPhamController', 
  ['$scope',   '$mdDialog', '$mdMedia',   '$firebaseArray', '$firebaseObject',  
  function($scope,   $mdDialog, $mdMedia,  $firebaseArray, $firebaseObject )
{

	$scope.curSP =[];
	$scope.curSP.TenSP = $scope.CurrentSanPhamEdit.TenSanPham;
	$scope.curSP.GiaSP = $scope.CurrentSanPhamEdit.GiaSanPham;
	$scope.curSP.LoaiSP = $scope.CurrentSanPhamEdit.LoaiSanPham;
	$scope.curSP.SoLuongSP = $scope.CurrentSanPhamEdit.SoLuongSanPham;
	$scope.curSP.MoTaSP = $scope.CurrentSanPhamEdit.MoTaSanPham;
	

	//chinh sua so luong bang san pham
	var refKeyThemSoLuong = $scope.CurrentSanPhamEdit.$id;
	var refThemSoLuong = new Firebase("https://bacphu.firebaseio.com/SanPham/"+ refKeyThemSoLuong);
	  		 		 
	 $scope.EditSanPhamSoLuongThem = [];
	 $firebaseObject(refThemSoLuong).$bindTo($scope, "EditSanPhamSoLuongThem");

	$scope.ThemLanNhapMoiSanPham = function()
	{
		 
		var refChiTietLanThemSanPham = new Firebase("https://bacphu.firebaseio.com/ChiTietLanThemSanPham");
		var LanThemSanPhamCuoi;
		var newLanThemMoi = refChiTietLanThemSanPham.push();

		 var listLanThem = $firebaseArray(refChiTietLanThemSanPham);


		 listLanThem.$loaded().then(function(listLanThem)
		 {
		 	 
 			 var newIDLanThem = $scope.CurrentSanPhamEdit.SoLanThem + 1;
 			  var currentDate = new Date()
		    var day = currentDate.getDate();
		    var month = currentDate.getMonth() + 1;
		    var year = currentDate.getFullYear();
		    var curDate = day + "/" + month + "/" + year;
		    var soluonglanthem =  parseInt($scope.SoLuongLanThem);

 			  newLanThemMoi.set(
 			 	{
 			 		IDSanPham: $scope.CurrentSanPhamEdit.ID,
 			 		IDLanThem: newIDLanThem,
 			 		SoLuongLanThem: soluonglanthem, 
 			 		NgayThem: curDate
 			 	});

 			  //up date so luong san pham bang san pham

 
		 	$scope.EditSanPhamSoLuongThem.SoLuongSanPham =
		 	 parseInt($scope.CurrentSanPhamEdit.SoLuongSanPham) + parseInt($scope.SoLuongLanThem);
		 	 $scope.EditSanPhamSoLuongThem.SoLanThem = parseInt($scope.EditSanPhamSoLuongThem.SoLanThem) + 1;

		 });
		 
	
	}

}]);



app.controller('AdminLanThemMoiSanPhamTheoLoaiController', 
  ['$scope',   '$mdDialog', '$mdMedia',   '$firebaseArray', '$firebaseObject',  
  function($scope,   $mdDialog, $mdMedia,  $firebaseArray, $firebaseObject )
{

	$scope.curSPTheoLoai =[];
	$scope.curSPTheoLoai.TenSP = $scope.CurrentSanPhamEditTheoLoai.TenSanPham;
	$scope.curSPTheoLoai.GiaSP = $scope.CurrentSanPhamEditTheoLoai.GiaSanPham;
	$scope.curSPTheoLoai.LoaiSP = $scope.CurrentSanPhamEditTheoLoai.LoaiSanPham;
	$scope.curSPTheoLoai.SoLuongSP = $scope.CurrentSanPhamEditTheoLoai.SoLuongSanPham;
	$scope.curSPTheoLoai.MoTaSP = $scope.CurrentSanPhamEditTheoLoai.MoTaSanPham;


	// chinh sua so luong bang san pham
	//   lay link url san pham theo loai
  	var refKeyTheoLoai = $scope.CurrentSanPhamEditTheoLoai.$id;
  
  
	var refEditSanPhamTheoLoai = new Firebase("https://bacphu.firebaseio.com/SanPham/"+ refKeyTheoLoai);
  		 		 
	$scope.EditSanPhamSoLuongThemTheoLoai = [];
 
  	 	 $firebaseObject(refEditSanPhamTheoLoai).$bindTo($scope, "EditSanPhamSoLuongThemTheoLoai");

	$scope.ThemLanNhapMoiSanPhamTheoLoai = function()
	{
		 
		var refChiTietLanThemSanPhamTheoLoai= new Firebase("https://bacphu.firebaseio.com/ChiTietLanThemSanPham");
		var LanThemSanPhamCuoiTheoLoai;
		var newLanThemMoiTheoLoai = refChiTietLanThemSanPhamTheoLoai.push();

		 var listLanThemTheoLoai = $firebaseArray(refChiTietLanThemSanPhamTheoLoai);


		 listLanThemTheoLoai.$loaded().then(function(listLanThemTheoLoai)
		 {
		 	 
 			 var newIDLanThemTheoLoai = $scope.CurrentSanPhamEditTheoLoai.SoLanThem + 1;
 			 var currentDate = new Date()
		    var day = currentDate.getDate();
		    var month = currentDate.getMonth() + 1;
		    var year = currentDate.getFullYear();
		    var curDate = day + "/" + month + "/" + year;
 			var soluonglanthemTheoLoai =  parseInt($scope.SoLuongLanThemTheoLoai);

 			  newLanThemMoiTheoLoai.set(
 			 	{
 			 		IDSanPham: $scope.CurrentSanPhamEditTheoLoai.ID,
 			 		IDLanThem: newIDLanThemTheoLoai,
 			 		SoLuongLanThem: soluonglanthemTheoLoai, 
 			 		NgayThem: curDate
 			 	});

 			  //up date so luong san pham bang san pham

 
		 	 $scope.EditSanPhamSoLuongThemTheoLoai.SoLuongSanPham =
		 	  parseInt($scope.CurrentSanPhamEditTheoLoai.SoLuongSanPham) + parseInt($scope.SoLuongLanThemTheoLoai);
		 	  
		 	$scope.EditSanPhamSoLuongThemTheoLoai.SoLanThem = parseInt($scope.EditSanPhamSoLuongThemTheoLoai.SoLanThem) + 1;

		 });
		 
	
	}

}]);




 
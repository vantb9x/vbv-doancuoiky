
var auth = firebase.auth();
var storageRef = firebase.storage().ref();
var file1, metadata1, file2, metadata2, file3, metadata3;

//SignIn Anonymously to firebase storage
auth.signInAnonymously().then(function (user) {
    console.log('Anonymous Sign In Success', user);
}).catch(function (error) {
    console.error('Anonymous Sign In Error', error);
});



app.controller('AdminTemplateSanPhamController',
  ['$scope', '$mdSidenav', '$mdDialog', '$mdMedia', '$mdBottomSheet', '$firebaseArray', '$routeParams', '$mdToast', '$filter', 
  function($scope, $mdSidenav, $mdDialog, $mdMedia, $mdBottomSheet, $firebaseArray, $routeParams, $mdToast, $filter)
  {	 //show list bottom

	 var refSanPham = new Firebase("https://bacphu.firebaseio.com/SanPham/");
	$scope.ListSanPham = $firebaseArray(refSanPham);
 
	 $scope.openBottemThemSanPham = function() {
	    	$mdBottomSheet.show({
	    		templateUrl: 'template/AdminBottomSheetAddSanPham.html',
	    		controller: 'AdminTemplateSanPhamController',
	    			scope: $scope,
					preserveScope: true

	    	});
	    	

	    }


	    	$scope.fileNameChanged1 = function (evt) {
	    	    
	    	    file1 = evt.files[0];
	    	    metadata1 = {
	    	        'contentType': file1.type
	    	    };
	    	    var reader = new FileReader();
	    	    reader.onload = function(e)
	    	    {
	    	    		$scope.$apply(function()
	    			{
	    				$scope.step1 = (e.target.result);
	    			});
	    	    }
	    	    reader.readAsDataURL(file1);
	    	}
	     
	    	$scope.fileNameChanged2 = function (evt) {
	    	    
	    	    file2 = evt.files[0];
	    	    metadata2 = {
	    	        'contentType': file2.type
	    	    };
	    	     var reader = new FileReader();
	    	    reader.onload = function(e)
	    	    {
	    	    		$scope.$apply(function()
	    			{
	    				$scope.step2 = (e.target.result);
	    			});
	    	    }
	    	    reader.readAsDataURL(file2);
	    	}
	    	$scope.fileNameChanged3 = function (evt) {
	    	   
	    	    file3 = evt.files[0];
	    	    metadata3 = {
	    	        'contentType': file3.type
	    	    };
	    	     var reader = new FileReader();
	    	    reader.onload = function(e)
	    	    {
	    	    		$scope.$apply(function()
	    			{
	    				$scope.step3 = (e.target.result);
	    			});
	    	    }
	    	    reader.readAsDataURL(file3);
	    	}

	$scope.AdminThemSanPham = function()
	{
	    	$scope.ListSanPham.$loaded().then(function(ListSanPham)
	    	{
   //              //Upload Image to Firebase storage
	    	    var uploadTask1 = storageRef.child('images/' + file1.name).put(file1, metadata1);
	    	      var uploadTask2 = storageRef.child('images/' + file2.name).put(file2, metadata2);
	    	     var uploadTask3 = storageRef.child('images/' + file3.name).put(file3, metadata3);

	    	    var url1, url2, url3;
 
	 	  	uploadTask1.on('state_changed', null, function(error)
	 	  		{
	 	  			console.error("upload failed 1", error);
	 	  		}, function()
	 	 		{
	 	 			url1 = uploadTask1.snapshot.metadata.downloadURLs[0];
	 	 			 
	 	 			uploadTask2.on('state_changed', null, function(error)
	 	 				{
	 	  					console.error("upload failed 2", error);
	 	  				}, function()
	 	 				{
	 	  					url2 = uploadTask2.snapshot.metadata.downloadURLs[0];
	 	 					 
	 	 					uploadTask3.on('state_changed', null, function(error)
	 	 						{
	 	  							console.error("upload failed 3", error);
	 	  						}, function()
	 	  						{	
	 	 							url3 = uploadTask3.snapshot.metadata.downloadURLs[0];
	 								 
    //them san pham
	     	    var lengthListSanPham = ListSanPham.length;
	    	    
	    	    var newID;
	    	    if (lengthListSanPham === 0) 
	    	    {
	    	        newID = 0;
	    	    }
	    	    else 
	    	    {
	    	        newID = parseInt(ListSanPham[lengthListSanPham - 1].ID) + 1;
	    	    }

	    	    var loaiSanPham = $scope.LoaiSanPham;
	    	    var tenSanPham = $scope.TenSanPham;
	    	    var giaSanPham = $scope.GiaSanPham;
	    	    var soLuongSanPham = parseInt($scope.SoLuongSanPham);
	    	    var moTaSanPham = $scope.MoTaSanPham;
	    	    var maSanPham = $scope.MaSanPham;

		 		var newSanPham = refSanPham.push();
   			 
		   		newSanPham.set(
	        	{
	           		ID: newID,
	            	LoaiSanPham: loaiSanPham,
	            	TenSanPham: tenSanPham,
	    		 	SoLuongSanPham: soLuongSanPham,
	    		 	GiaSanPham: giaSanPham,
	    			MoTaSanPham: moTaSanPham,
	    			MaSanPham: maSanPham,
	     	 		 
	     	 		ImagePhu1:  url1,
	    	 		ImagePhu2:  url2,
	           		ImagePhu3: url3,
	           		SoLanThem: 1, 
	           		SoLuongBan: 0
	        	},function(error) 
	        	{
	        		if (error) 
	        		{
	            		$mdToast.show(
	         			$mdToast.simple().textContent('Thêm sản phẩm bị lỗi')
	         			.position('top right')
	         			.hideDelay(5000));
	        		} 
	        		else 
	        		{
	        			$mdToast.show(
	         			$mdToast.simple().textContent('Đã thêm 1 sản phẩm mới')
	         			.position('top right')
	         			.hideDelay(5000));
	         		}
	        	});

		   	//them vao chi tiet lan them san pham
		   	var refChiTietLanThemSanPham = new Firebase("https://bacphu.firebaseio.com/ChiTietLanThemSanPham");
	   		var newLanThemSanPham = refChiTietLanThemSanPham.push();
	   		//	$scope.ListLanThemSanPham = $firebaseArray(refChiTietLanThemSanPham);

		    var currentDate = new Date()
	    	var day = currentDate.getDate();
	    	var month = currentDate.getMonth() + 1;
	    	var year = currentDate.getFullYear();
	    	var curDate = day + "/" + month + "/" + year;

	   		newLanThemSanPham.set(
	   		{
	   			IDSanPham: newID,
	   			IDLanThem: 1,
	   			SoLuongLanThem: soLuongSanPham,
	   			NgayThem: curDate
	   		});

   	        $scope.LoaiSanPham = "";
   	        $scope.TenSanPham = "";
   	        $scope.GiaSanPham = "";
   	        $scope.SoLuongSanPham = "";
   	        $scope.MoTaSanPham = "";
   	        $scope.MaSanPham = "";
   	        
   	        $scope.step1 = "";
   	        $scope.step2 = "";
   	        $scope.step3 = "";

   // //url
   // 				//het them san pham
			$mdBottomSheet.hide();

	 	 						});
	 	 				});
	 	 		});
 

	      //an thanh bottom them san pham
		
	});
}

 
	 $scope.status = '  ';
	  $scope.customFullscreen = $mdMedia('xs') || $mdMedia('sm');
	 
	 $scope.TimIDSanPham = function(item)
	 {
	 	var index = KiemTraPhanTuTonTaiTheoID($scope.ListSanPham, item);
	 	$scope.CurrentSanPhamEdit = $scope.ListSanPham[index];
	 	 
	 }
	 
	 $scope.showEditSanPhamDialog = function(ev) 
	 {

	   	   var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;

	     	$mdDialog.show(
	   		   {
			      	clickOutsideToClose: true,
			        controller: DialogEditSanPhamController,
			        templateUrl: 'template/AdminTemplateChinhSuaSanPham.html',
			        scope: $scope,
			        preserveScope: true,
			        parent: angular.element(document.body),
			        targetEvent: ev,
			        
			        fullscreen: useFullScreen
	      		})
	     		.then(function(answer) 
	     		{
    			    
    			   if (answer === "Cancel")
    			   {
    			   	 	 $mdDialog.hide();
    			   }
    			}, function()
    			{
     				 $scope.status = 'You cancelled the dialog.';
   		 		});


  		$scope.$watch(function() 
  		{
     		 return $mdMedia('xs') || $mdMedia('sm');
    	}, function(wantsFullScreen) 
    	{
      		$scope.customFullscreen = (wantsFullScreen === true);
   		});
 
	  };

	  $scope.xoaSanPham = function(ev, item)
	  {
	  	alert(item.TenSanPham);
	  	 var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;

	     $mdDialog.show(
	   	{
			      	clickOutsideToClose: true,
			        controller: DialogEditSanPhamController,
			        templateUrl: 'template/AdminTemplateDialogXoaSanPham.html',
			        parent: angular.element(document.body),
			        targetEvent: ev,
			          scope: $scope,
			        preserveScope: true,
			        fullscreen: useFullScreen
	     })
	     .then(function(answer) 
	     {
    	  if (answer === "OK")
    	  {
    	  	//alert($scope.ListSanPham.length);
    	  	// $scope.ListSanPham.$remove(item);
    	  	var refKeyDelete = item.$id;
  
			var refDelete = new Firebase("https://bacphu.firebaseio.com/SanPham/"+ refKeyDelete);
			refDelete.remove();


    	  	 $mdToast.show(
			         $mdToast.simple().textContent('Xóa sản phẩm thành công')
			         .position('top right')
			         .hideDelay(5000)

			        );
    	  }
    	}, function()
    	{
     		$scope.status = 'You cancelled the dialog.';
   		 });


  		$scope.$watch(function() 
  		{
     		 return $mdMedia('xs') || $mdMedia('sm');
    	}, function(wantsFullScreen) 
    	{
      		$scope.customFullscreen = (wantsFullScreen === true);
   		});
	  }

 $scope.showThemLanNhapSanPham = function(ev) 
	 {

	   	   var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;

	     	$mdDialog.show(
	   		   {
			      	clickOutsideToClose: true,
			        controller: DialogEditSanPhamController,
			        templateUrl: 'template/AdminTemplateThemLanNhapSanPham.html',
			        scope: $scope,
			        preserveScope: true,
			        parent: angular.element(document.body),
			        targetEvent: ev,
			        
			        fullscreen: useFullScreen
	      		})
	     		.then(function(answer) 
	     		{
    			    
    			   if (answer === "Cancel")
    			   {
    			   	 	 $mdDialog.hide();
    			   }
    			}, function()
    			{
     				 $scope.status = 'You cancelled the dialog.';
   		 		});


  		$scope.$watch(function() 
  		{
     		 return $mdMedia('xs') || $mdMedia('sm');
    	}, function(wantsFullScreen) 
    	{
      		$scope.customFullscreen = (wantsFullScreen === true);
   		});
 
 
	  };


	   $scope.XemChiTietLanNhap = function(ev) 
	 {

	   	   var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;

	     	$mdDialog.show(
	   		   {
			      	clickOutsideToClose: true,
			        controller: DialogEditSanPhamController,
			        templateUrl: 'template/AdminXemChiTietLanNhapSanPham.html',
			        scope: $scope,
			        preserveScope: true,
			        parent: angular.element(document.body),
			        targetEvent: ev,
			        
			        fullscreen: useFullScreen
	      		})
	     		.then(function(answer) 
	     		{
    			    
    			   if (answer === "Cancel")
    			   {
    			   	 	 $mdDialog.hide();
    			   }
    			}, function()
    			{
     				 $scope.status = 'You cancelled the dialog.';
   		 		});


  		$scope.$watch(function() 
  		{
     		 return $mdMedia('xs') || $mdMedia('sm');
    	}, function(wantsFullScreen) 
    	{
      		$scope.customFullscreen = (wantsFullScreen === true);
   		});
 
 
	  };


	$scope.flagTheoTen = false;
 	$scope.SapXepSanPhamTheoTen = function()
 	{
 		if ($scope.flagTheoTen === true)
 		{
 		$scope.ListSanPham = $filter('orderBy')($scope.ListSanPham, 'TenSanPham', true);
 		$scope.flagTheoTen = false;
 		 }
 		 else
 		 {
 		 		$scope.ListSanPham = $filter('orderBy')($scope.ListSanPham, 'TenSanPham', false);
 		 		$scope.flagTheoTen  = true;
 		 }
 	}

 	$scope.flagTheoGia = false;
 	$scope.SapXepSanPhamTheoGia = function()
 	{
 		if ($scope.flagTheoGia === true)
 		{
 		$scope.ListSanPham = $filter('orderBy')($scope.ListSanPham, 'GiaSanPham', true);
 		$scope.flagTheoGia = false;
 		 }
 		 else
 		 {
 		 		$scope.ListSanPham = $filter('orderBy')($scope.ListSanPham, 'GiaSanPham', false);
 		 		$scope.flagTheoGia  = true;
 		 }
 	}

 	$scope.flagTheoTongHang = false;
 	$scope.SapXepSanPhamTheoTongHang = function()
 	{
 		if ($scope.flagTheoTongHang === true)
 		{
 		$scope.ListSanPham = $filter('orderBy')($scope.ListSanPham, 'SoLuongSanPham', true);
 		$scope.flagTheoTongHang = false;
 		 }
 		 else
 		 {
 		 		$scope.ListSanPham = $filter('orderBy')($scope.ListSanPham, 'SoLuongSanPham', false);
 		 		$scope.flagTheoTongHang  = true;
 		 }
 	}

 	 	$scope.flagTheoDaBan = false;
 	$scope.SapXepSanPhamTheoDaBan = function()
 	{
 		if ($scope.flagTheoDaBan === true)
 		{
 		$scope.ListSanPham = $filter('orderBy')($scope.ListSanPham, 'SoLuongBan', true);
 		$scope.flagTheoDaBan = false;
 		 }
 		 else
 		 {
 		 	$scope.ListSanPham = $filter('orderBy')($scope.ListSanPham, 'SoLuongBan', false);
 		 	$scope.flagTheoDaBan  = true;
 		 }
 	}

 	 	$scope.flagTheoMaSanPham = false;
 	$scope.SapXepSanPhamTheoMaSanPham = function()
 	{
 		if ($scope.flagTheoMaSanPham === true)
 		{
 		$scope.ListSanPham = $filter('orderBy')($scope.ListSanPham, 'MaSanPham', true);
 		$scope.flagTheoMaSanPham = false;
 		 }
 		 else
 		 {
 		 	$scope.ListSanPham = $filter('orderBy')($scope.ListSanPham, 'MaSanPham', false);
 		 	$scope.flagTheoMaSanPham  = true;
 		 }
 	}

 	 	$scope.flagTheoID = false;
 	$scope.SapXepSanPhamTheoID = function()
 	{
 		if ($scope.flagTheoID === true)
 		{
 		$scope.ListSanPham = $filter('orderBy')($scope.ListSanPham, 'ID', true);
 		$scope.flagTheoID = false;
 		 }
 		 else
 		 {
 		 	$scope.ListSanPham = $filter('orderBy')($scope.ListSanPham, 'ID', false);
 		 	$scope.flagTheoID  = true;
 		 }
 	}

	$scope.flagTheoLoaiSanPham = false;
 	$scope.SapXepSanPhamTheoLoaiSanPham = function()
 	{
 		if ($scope.flagTheoLoaiSanPham === true)
 		{
 		$scope.ListSanPham = $filter('orderBy')($scope.ListSanPham, 'LoaiSanPham', true);
 		$scope.flagTheoLoaiSanPham = false;
 		 }
 		 else
 		 {
 		 	$scope.ListSanPham = $filter('orderBy')($scope.ListSanPham, 'LoaiSanPham', false);
 		 	$scope.flagTheoLoaiSanPham  = true;
 		 }
 	}
}]);

 

function DialogEditSanPhamController($scope, $mdDialog) {
  $scope.hide = function() {
    $mdDialog.hide();
  };
  $scope.cancel = function() {
    $mdDialog.cancel();
  };
  $scope.answer = function(answer) {
    $mdDialog.hide(answer);
  };
}

function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#imgInp").change(function(){
    readURL(this);
});


function KiemTraPhanTuTonTaiTheoID(array, item)
{
  var length = array.length;
  
  for (var i = 0; i < length; i++)
  {
    if (array[i].ID === item.ID)
    {
      return i;
    }
  }
  return -1;
}

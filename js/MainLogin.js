function MainLoginController($scope, $mdDialog, $firebaseArray, $mdToast, $mdMedia, AuthFacebook,authFactory)
{
  $scope.hide = function() {
    $mdDialog.hide();
  };

  $scope.cancel = function() {
    $mdDialog.cancel();
  };

  $scope.answer = function(answer) {
    $mdDialog.hide(answer);
  };
  $scope.MainDangKyTaiKhoan = function(ev)
  {
   
       var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;

       $mdDialog.show(
        {
              clickOutsideToClose: true,
              controller: MainLoginController,
              templateUrl: 'template/MainDangKyTaiKhoan.html',
              parent: angular.element(document.body),
              targetEvent: ev,
              scope: $scope,
          preserveScope: true,
              fullscreen: useFullScreen
            })
          .then(function(answer) 
          {
            
          }, function()
          {
             $scope.status = 'You cancelled the dialog.';
             //hien dialog xem chi tiet ssan pham lai
             
          });


      $scope.$watch(function() 
      {
         return $mdMedia('xs') || $mdMedia('sm');
      }, function(wantsFullScreen) 
      {
          $scope.customFullscreen = (wantsFullScreen === true);
      });

  }

  $scope.MainLoginBinhThuong = function()
{
  

       var user = $scope.loginUserName + "@gmail.com";
           var password = $scope.loginPassword;
       
   var result = authFactory.authUser(user, password);
  
      result.then(function(authDataBT){
        //dangnhap thanh cong
         console.log("User Successfully logged in with uid: ", authDataBT.uid)
 
         var refTaiKhoanDN = new Firebase("https://bacphu.firebaseio.com/KhachHang");
       refTaiKhoanDN.orderByChild("TaiKhoan").equalTo(user).on("child_added", function(snapshot)
          {
            $scope.TenTaiKhoanDangNhap = snapshot.val().TenKhachHang;
          
             
          });
      

         $mdDialog.hide();
          
      }, function(error) {
        console.log("Authentication Failed: ", error)
        alert("Tài khoản chưa đúng");
      });
}

 
}

app.controller('AppCtrlLogin', ["$scope", "$firebaseAuth", "authFactory", "$mdDialog", "$firebaseArray", "$mdToast",
  function($scope, $firebaseAuth, authFactory, $mdDialog, $firebaseArray, $mdToast)
 {
      var ref = new Firebase("https://bacphu.firebaseio.com/");
    auth = $firebaseAuth(ref);
 
 $scope.GioiTinh = "Nam";
    $scope.DangKyTaiKhoan = function()
  {
    
      $scope.message = null;
        $scope.error = null;

        //tao khach hang trong trang admin
          
           var nameDK = $scope.NameDK;
           var userDK = $scope.UserDK + "@gmail.com";
           var passwordDK = $scope.PasswordDK;
           var password1DK = $scope.Password1DK;
           var dienthoaiDK = $scope.DienThoaiDK;
           var diachiDK = $scope.DiaChiDK;
           var gioitinh = $scope.GioiTinh;
           var ngaysinh = $scope.NgaySinhDate.toString();
         
           var ngaysinhori =  ngaysinh.split(":")[0];
          var lengthngaysinh = ngaysinhori.length;
          var nsformat = ngaysinhori.substring(4, lengthngaysinh-3);
                 
 if (passwordDK !=  password1DK)
 {
  alert("2 mật khẩu khác nhau.");
 }
 else
 {
    var refKhachHang = new Firebase("https://bacphu.firebaseio.com/KhachHang");
        $scope.ListKhachHang = $firebaseArray(refKhachHang);
       
      $scope.ListKhachHang.$loaded().then(function(ListKhachHang) {
        var ListKhachHangLength = ListKhachHang.length; 
         var newIDKhachHang;

        if (ListKhachHangLength === 0)
        {
          newIDKhachHang = 0;
        }
        else
        {
          newIDKhachHang = parseInt(ListKhachHang[ListKhachHangLength - 1].IDKhachHang) + 1;
        }
      
        var newKhachHang = refKhachHang.push();
           newKhachHang.set(
            {
              IDKhachHang : newIDKhachHang,
              TenKhachHang : nameDK,
              DienThoaiKhachHang: dienthoaiDK,
              DiaChiKhachHang: diachiDK,
              GioiTinh: gioitinh,
              NgaySinh: nsformat,
              TaiKhoan: userDK
            },function(error) 
            {
              if (error) 
              {
                  $mdToast.show(
                $mdToast.simple().textContent('Đăng ký tài khoản bị lỗi')
                .position('top right')
                .hideDelay(5000));
              } 
              else 
              {
                $mdToast.show(
                $mdToast.simple().textContent('Đăng ký tài khoản thành công')
                .position('top right')
                .hideDelay(5000));
              }
            });
 
             
           }
            
       );
   
       auth.$createUser({
         email: userDK,
          password: passwordDK
      }).then(function(userData) {
        $scope.message = "User created with uid: " + userData.uid;
        
      }).catch(function(error) {
        $scope.error = error;
       
       });

         $mdDialog.hide();
       }
  }


 //     $scope.AdminLogin = function() {

 //        var user = $scope.username;
 //           var password = $scope.userpassword;
       
 //  var result = authFactory.authUser(user, password);
 //      result.then(function(authData){
 //         console.log("User Successfully logged in with uid: ", authData.uid)
 //          var url = window.location.href;
 //            var length = url.length;
 //             var urlorigin = window.location.href.substring(0, length - 10);
 //              var adminUrl = urlorigin + "admin.html#/sanpham";
               
 //              window.location.href = adminUrl;  
 //      }, function(error) {
 //        console.log("Authentication Failed: ", error)
 //      })
 //    };
 
   
}]);
      


 
app.controller('AdminTemplateSanPhamTheoLoaiController', 
  ['$scope', '$mdSidenav', '$mdDialog', '$mdMedia', '$mdBottomSheet', '$firebaseArray', '$routeParams', '$mdToast', '$filter', 
  function($scope, $mdSidenav, $mdDialog, $mdMedia, $mdBottomSheet, $firebaseArray, $routeParams, $mdToast, $filter)
{
  var refSanPham = new Firebase("https://bacphu.firebaseio.com/SanPham/");
  
   $scope.ListSanPhamTL = $firebaseArray(refSanPham);
  $scope.ListSanPhamTheoLoai = [];
  refSanPham.orderByChild("LoaiSanPham").equalTo($routeParams.loaiSanPham).on("child_added", function(snapshot)
  	{
  		$scope.ListSanPhamTheoLoai.push(snapshot.val());
  	});
 

 
	  $scope.openBottemThemSanPham = function() {
	  
	 
	    	$mdBottomSheet.show({
	    		templateUrl: 'template/AdminBottomSheetAddSanPham.html',
	    		controller: 'AdminTemplateSanPhamController',
	    			scope: $scope,
					preserveScope: true
	    	});
	    }

 


 	$scope.status = '  ';
	 $scope.customFullscreen = $mdMedia('xs') || $mdMedia('sm');
 
	 $scope.TimIDSanPhamTheoLoai = function(item)
	 {
	 	var index = KiemTraPhanTuTonTaiTheoIDTheoLoai($scope.ListSanPhamTL, item);
	 	// $scope.IDEditSPTheoLoai = index;
	 	// $scope.LoaiSanPhamEdit = $routeParams.loaiSanPham;
	 	$scope.CurrentSanPhamEditTheoLoai = $scope.ListSanPhamTL[index];
	  
	 	 
	 }
	 
	 $scope.showEditSanPhamDialogTheoLoai = function(ev) 
	 {

	   	   var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;

	     	$mdDialog.show(
	   		   {
			      	clickOutsideToClose: true,
			        controller: DialogEditSanPhamController,
			        templateUrl: 'template/AdminTemplateChinhSuaSanPhamTheoLoai.html',
			        scope: $scope,
			        preserveScope: true,
			        parent: angular.element(document.body),
			        targetEvent: ev,
			        
			        fullscreen: useFullScreen
	      		})
	     		.then(function(answer) 
	     		{
    			  
    			}, function()
    			{
     				 $scope.status = 'You cancelled the dialog.';
   		 		});


  		$scope.$watch(function() 
  		{
     		 return $mdMedia('xs') || $mdMedia('sm');
    	}, function(wantsFullScreen) 
    	{
      		$scope.customFullscreen = (wantsFullScreen === true);
   		});
 
	  };



  $scope.xoaSanPhamTheoLoai = function(ev, item)
	  {
	  	 var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;

	     $mdDialog.show(
	   	{
			      	clickOutsideToClose: true,
			        controller: DialogEditSanPhamController,
			        templateUrl: 'template/AdminTemplateDialogXoaSanPham.html',
			        parent: angular.element(document.body),
			        targetEvent: ev,
			        scope: $scope,
					preserveScope: true,
			        fullscreen: useFullScreen
	     })
	     .then(function(answer) 
	     {
    	  if (answer === "OK")
    	  {
    	  		 
    	 //  		var indexItem = $scope.IDEditSPTheoLoai;

    	 //  		var refUrlItem;
    	 //  		var i = 0; 
    	 //  		 refSanPham.orderByChild("LoaiSanPham")
    	 //  		 .equalTo($routeParams.loaiSanPham)
    	 //  		 .on("child_added", function(snapshot)
  				// {
  				// 	if (i === indexItem)
  				// 	{
  				// 		refUrlItem = snapshot.key();
  				// 	}
  				// });

    	  		 
    	 //  		var refUrlItemDelete = refSanPham.child(refUrlItem);
    	  		 
    	 //  		refUrlItemDelete.remove();


 			var refKeyDeleteTheoLoai = $scope.CurrentSanPhamEditTheoLoai.$id;
  	 
			var refDeleteTheoLoai = new Firebase("https://bacphu.firebaseio.com/SanPham/"+ refKeyDeleteTheoLoai);
			refDeleteTheoLoai.remove();

  				 $mdToast.show(
			         $mdToast.simple().textContent('Xóa sản phẩm thành công!')
			         .position('top right')
			         .hideDelay(5000)

			        );
    	  }
    	}, function()
    	{
     		$scope.status = 'You cancelled the dialog.';
   		 });


  		$scope.$watch(function() 
  		{
     		 return $mdMedia('xs') || $mdMedia('sm');
    	}, function(wantsFullScreen) 
    	{
      		$scope.customFullscreen = (wantsFullScreen === true);
   		});
	  }
	  //them lan nhap san pham theo loai
	 $scope.showThemLanNhapSanPhamTheoLoai = function(ev) 
	 {

	   	   var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;

	     	$mdDialog.show(
	   		   {
			      	clickOutsideToClose: true,
			        controller: DialogEditSanPhamController,
			        templateUrl: 'template/AdminTemplateThemLanNhapSanPhamTheoLoai.html',
			        scope: $scope,
			        preserveScope: true,
			        parent: angular.element(document.body),
			        targetEvent: ev,
			        
			        fullscreen: useFullScreen
	      		})
	     		.then(function(answer) 
	     		{
    			    
    			   if (answer === "Cancel")
    			   {
    			   	 	 $mdDialog.hide();
    			   }
    			}, function()
    			{
     				 $scope.status = 'You cancelled the dialog.';
   		 		});


  		$scope.$watch(function() 
  		{
     		 return $mdMedia('xs') || $mdMedia('sm');
    	}, function(wantsFullScreen) 
    	{
      		$scope.customFullscreen = (wantsFullScreen === true);
   		});
 
 
	  };

	     $scope.XemChiTietLanNhapTheoLoai= function(ev) 
	 {

	   	   var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;

	     	$mdDialog.show(
	   		   {
			      	clickOutsideToClose: true,
			        controller: DialogEditSanPhamController,
			        templateUrl: 'template/AdminXemChiTietLanNhapSanPhamTheoLoai.html',
			        scope: $scope,
			        preserveScope: true,
			        parent: angular.element(document.body),
			        targetEvent: ev,
			        
			        fullscreen: useFullScreen
	      		})
	     		.then(function(answer) 
	     		{
    			    
    			   if (answer === "Cancel")
    			   {
    			   	 	 $mdDialog.hide();
    			   }
    			}, function()
    			{
     				 $scope.status = 'You cancelled the dialog.';
   		 		});


  		$scope.$watch(function() 
  		{
     		 return $mdMedia('xs') || $mdMedia('sm');
    	}, function(wantsFullScreen) 
    	{
      		$scope.customFullscreen = (wantsFullScreen === true);
   		});
 
 
	  };

	  $scope.flagTheoTenTheoLoai = false;
 	$scope.SapXepSanPhamTheoTenTheoLoai = function()
 	{

 		if ($scope.flagTheoTenTheoLoai === true)
 		{
 		$scope.ListSanPhamTheoLoai = $filter('orderBy')($scope.ListSanPhamTheoLoai, 'TenSanPham', true);
 		$scope.flagTheoTenTheoLoai = false;
 		 }
 		 else
 		 {
 		 		$scope.ListSanPhamTheoLoai = $filter('orderBy')($scope.ListSanPhamTheoLoai, 'TenSanPham', false);
 		 		$scope.flagTheoTenTheoLoai  = true;
 		 }
 	}

 	$scope.flagTheoGiaTheoLoai = false;
 	$scope.SapXepSanPhamTheoGiaTheoLoai = function()
 	{
 		if ($scope.flagTheoGiaTheoLoai === true)
 		{
 		$scope.ListSanPhamTheoLoai = $filter('orderBy')($scope.ListSanPhamTheoLoai, 'GiaSanPham', true);
 		$scope.flagTheoGiaTheoLoai = false;
 		 }
 		 else
 		 {
 		 		$scope.ListSanPhamTheoLoai = $filter('orderBy')($scope.ListSanPhamTheoLoai, 'GiaSanPham', false);
 		 		$scope.flagTheoGiaTheoLoai  = true;
 		 }
 	}

 	$scope.flagTheoTongHangTheoLoai = false;
 	$scope.SapXepSanPhamTheoTongHangTheoLoai = function()
 	{
 		if ($scope.flagTheoTongHangTheoLoai === true)
 		{
 		$scope.ListSanPhamTheoLoai = $filter('orderBy')($scope.ListSanPhamTheoLoai, 'SoLuongSanPham', true);
 		$scope.flagTheoTongHangTheoLoai = false;
 		 }
 		 else
 		 {
 		 		$scope.ListSanPhamTheoLoai = $filter('orderBy')($scope.ListSanPhamTheoLoai, 'SoLuongSanPham', false);
 		 		$scope.flagTheoTongHangTheoLoai  = true;
 		 }
 	}

 	 	$scope.flagTheoDaBanTheoLoai = false;
 	$scope.SapXepSanPhamTheoDaBanTheoLoai = function()
 	{
 		if ($scope.flagTheoDaBanTheoLoai === true)
 		{
 		$scope.ListSanPhamTheoLoai = $filter('orderBy')($scope.ListSanPhamTheoLoai, 'SoLuongBan', true);
 		$scope.flagTheoDaBanTheoLoai = false;
 		 }
 		 else
 		 {
 		 	$scope.ListSanPhamTheoLoai = $filter('orderBy')($scope.ListSanPhamTheoLoai, 'SoLuongBan', false);
 		 	$scope.flagTheoDaBanTheoLoai  = true;
 		 }
 	}
    $scope.flagTheoMaSanPhamTheoLoai = false;
  $scope.SapXepSanPhamTheoMaSanPhamTheoLoai = function()
  {
    if ($scope.flagTheoMaSanPhamTheoLoai === true)
    {
    $scope.ListSanPhamTheoLoai = $filter('orderBy')($scope.ListSanPhamTheoLoai, 'MaSanPham', true);
    $scope.flagTheoMaSanPhamTheoLoai = false;
     }
     else
     {
      $scope.ListSanPhamTheoLoai = $filter('orderBy')($scope.ListSanPhamTheoLoai, 'MaSanPham', false);
      $scope.flagTheoMaSanPhamTheoLoai  = true;
     }
  }

    $scope.flagTheoIDTheoLoai = false;
  $scope.SapXepSanPhamTheoIDTheoLoai = function()
  {
    if ($scope.flagTheoIDTheoLoai === true)
    {
    $scope.ListSanPhamTheoLoai = $filter('orderBy')($scope.ListSanPhamTheoLoai, 'ID', true);
    $scope.flagTheoIDTheoLoai = false;
     }
     else
     {
      $scope.ListSanPhamTheoLoai = $filter('orderBy')($scope.ListSanPhamTheoLoai, 'ID', false);
      $scope.flagTheoIDTheoLoai  = true;
     }
  }

  $scope.flagTheoLoaiSanPhamTheoLoai = false;
  $scope.SapXepSanPhamTheoLoaiSanPhamTheoLoai = function()
  {
    if ($scope.flagTheoLoaiSanPhamTheoLoai === true)
    {
    $scope.ListSanPhamTheoLoai = $filter('orderBy')($scope.ListSanPhamTheoLoai, 'LoaiSanPham', true);
    $scope.flagTheoLoaiSanPhamTheoLoai = false;
     }
     else
     {
      $scope.ListSanPhamTheoLoai = $filter('orderBy')($scope.ListSanPhamTheoLoai, 'LoaiSanPham', false);
      $scope.flagTheoLoaiSanPhamTheoLoai  = true;
     }
  }
	  

}]);

 

function DialogEditSanPhamTheoLoaiController($scope, $mdDialog) {
  $scope.hide = function() {
    $mdDialog.hide();
  };
  $scope.cancel = function() {
    $mdDialog.cancel();
  };
  $scope.answer = function(answer) {
    $mdDialog.hide(answer);
  };
}


function KiemTraPhanTuTonTaiTheoIDTheoLoai(array, item)
{
  var length = array.length;
  
  for (var i = 0; i < length; i++)
  {
    if (array[i].ID === item.ID)
    {
      return i;
    }
  }
  return -1;
}

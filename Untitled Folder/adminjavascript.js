		var app = angular.module('myApp1',['ngMaterial' ]);
			app.config(function($mdThemingProvider) {
				$mdThemingProvider.theme('default')
				.primaryPalette('pink', {
		      'default': '400', // by default use shade 400 from the pink palette for primary intentions
		      'hue-1': '100', // use shade 100 for the <code>md-hue-1</code> class
		      'hue-2': '600', // use shade 600 for the <code>md-hue-2</code> class
		      'hue-3': 'A100' // use shade A100 for the <code>md-hue-3</code> class
		  })
		    // If you specify less than all of the keys, it will inherit from the
		    // default shades
		    .accentPalette('cyan', {
		      'default': '500' // use shade 200 for default, and keep all other shades the same
		  });
		});

 
	app.controller('AppCtrl1', ['$scope', '$mdSidenav', '$mdDialog', '$mdMedia', '$mdBottomSheet', '$timeout', '$mdToast', function($scope, $mdSidenav, $mdDialog, $mdMedia, $mdBottomSheet, $timeout, $mdToast)
	{
			 
		 var originatorEv;
		 $scope.openMenu = function($mdOpenMenu, ev)
		 {
		 	originatorEv  = ev;
		 	$mdOpenMenu(ev);
		 }

	 //show list bottom
	 
  
	  $scope.openlist = function() {
	    	$mdBottomSheet.show({
	    		templateUrl: 'bottomSheetAddSanPham.html'
	    	});
	    }

	    $scope.AdminThemSanPham = function()
	    {
	    	 $mdBottomSheet.hide();
	    }
	}]);


app.directive('ngForma', function() {
  return {
   
    templateUrl : 'themSanPham.html'
  };
});
 

 
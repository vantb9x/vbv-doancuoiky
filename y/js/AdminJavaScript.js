    var app = angular.module('myApp1',['ngMaterial', 'ngRoute', 'firebase','angular-notification-icons']);
      app.config(function($mdThemingProvider) {
        $mdThemingProvider.theme('default')
        .primaryPalette('pink', {
          'default': '400', // by default use shade 400 from the pink palette for primary intentions
          'hue-1': '100', // use shade 100 for the <code>md-hue-1</code> class
          'hue-2': '600', // use shade 600 for the <code>md-hue-2</code> class
          'hue-3': 'A100' // use shade A100 for the <code>md-hue-3</code> class
      })
        // If you specify less than all of the keys, it will inherit from the
        // default shades
        .accentPalette('cyan', {
          'default': '500' // use shade 200 for default, and keep all other shades the same
      });
    });

 
  app.controller('AppCtrl1', ['$scope', '$mdSidenav', '$mdDialog',
   '$mdMedia', '$firebaseArray', '$routeParams', '$filter', "AuthFacebook",
    function($scope, $mdSidenav, $mdDialog, $mdMedia,
     $firebaseArray, $routeParams, $filter, AuthFacebook)
  {





      $scope.openSidenavLeft = function(sidenavid)
      {
        $mdSidenav(sidenavid).toggle();
      } 


     var originatorEv;
     $scope.openMenu = function($mdOpenMenu, ev)
     {
      originatorEv  = ev;
      $mdOpenMenu(ev);
     }

     $scope.DanhMucSanPham = [
    {
      loaisanpham: "Nhẫn"
    },
    {
  loaisanpham: "Dây chuyền"
    },
    {
   loaisanpham: "Lắc tay"
    },
    {
   loaisanpham: "Lắc chân"
    },
    {
         loaisanpham: "Hoa tai"
    }];

    $scope.ThongTinSanPham = [
    {
  title: "Loại sản phẩm"
    },
    {
  title: "Tên sản phẩm"
    },
    {
  title: "Giá"
    },
    {
  title: "Số lượng"
    },
    {
        title: "Mô tả"
    }, 
{
  title: "Mã sản phẩm"
}];

    $scope.TenShop = "PQ Silver";
   $scope.User = {
    Name: "Trần Thị Chi",
    Avatar: "image/user.jpg"
   };
   $scope.DanhMucSidenav = [
   {
      Name: "Sản phẩm",
      Icon: "dashboard"
   },
   {
      Name: "Đơn hàng",
      Icon: "shopping_cart"
   },
   {
      Name: "Tin nhắn",
      Icon: "email"
   },
   {
      Name: "Khách hàng",
      Icon: "people"
   },
   {
      Name: "Chỉnh sửa danh mục",
      Icon: "dashboard",
  children: {
  gioithieu: "Giới thiệu",
  lienhe: "Liên hệ",
  dosizenhan : "Đo size nhẫn"
}
   }];

   $scope.ItemToolbarRight = 
   {
      shopping : "shopping_cart",
      email: "email",
      person: {
       icon: "person",
       item : [
       {
          icon: "person",
          title: "User profile"
       },
       {
          icon: "settings",
          title: "Settings"
       },
       {
          icon: "exit_to_app",
          title: "Log out"
       }]
      }
   };

   $scope.ChinhSuaSanPham = 
   {
      luu: "Lưu",
      huy: "Hủy"
   };
  $scope.DataXoaSanPham = 
   {
    title: "Xóa sản phẩm",
    content: "Bạn có muốn xóa sản phẩm này?",
    deleteitem: "Xóa sản phẩm",
    cancel: "Hủy"
   };

   $scope.DataTableDonHang = {
    id : "ID",
    khachhang: "Khách hàng",
    soluong: "Số lượng",
    ngaymua: "Ngày mua",
    dienthoai: "Điện thoại",
    diachi: "Địa chỉ",
    dagiao: "Đã giao"
   };

   $scope.DataTableSanPham = {
    id: "ID",
    masanpham: "Mã sản phẩm",
    ten: "Tên sản phẩm",
    loai: "Loại",
    gia: "Giá",
    tonghang: "Tổng số",
    daban: "Đã bán",
    mota: "Mô tả",
    themhang: "Thêm hàng",
    chinhsua: "Chỉnh sửa",
    xoa: "Xóa"
   };

   $scope.DataLanNhapThemSanPham = 
   {
      title: "Nhập thêm sản phẩm",
      soluongthem: "Số lượng thêm",
      them: "Thêm",
      huy: "Hủy"
   };
   $scope.DataTableTinNhan = 
   {
      id: "ID",
      nguoigui: "Người gửi",
      noidung: "Nội dung",
      dienthoai: "Điện thoại",
      email: "Email",
      ngaygui: "Ngày gửi",
      phanhoi: "Phản hồi"
   };
   $scope.DataTableChiTietDonHang =
   {
      idsanpham: "ID sản phẩm",
      tensanpham: "Tên sản phẩm",
      soluong: "Số lượng", 
      giasanpham: "Giá",
      thanhtiensanpham: "Thành tiền",
      masanpham: "Mã sản phẩm"
  };

  $scope.DataChiTietLanNhapSanPham =
  {

    danhmuc: "Danh mục",
    soluong: "Số lượng",
    lannhap: "Lần nhập",
    ngaynhap: "Ngày nhập",
    tongsanpham: "Tổng sản phẩm",
    soluongban: "Số lượng bán",
    tonkho: "Tồn kho"
  };

	$scope.currentVN = "VNĐ"; 
	$scope.TongCongThanhToan = "Tổng cộng";

  var refDonHang = new Firebase("https://bacphu.firebaseio.com/DonHang");
  $scope.CountDonHangMoi = "0";
 // $scope.listdh = [];
   refDonHang.orderByChild("TinhTrangDaGiao").equalTo(false).on("child_added", function(snapshot)
    {
      // $scope.listdh.push(snapshot.val());
       $scope.CountDonHangMoi++;
      // alert(snapshot.key());
     }); 
  
   var refTinNhan = new Firebase("https://bacphu.firebaseio.com/TinNhan");
  $scope.CountTinNhanMoi = "0";
 // $scope.listdh = [];
   refTinNhan.orderByChild("TinhTrang").equalTo(false).on("child_added", function(snapshot)
    {
      
       $scope.CountTinNhanMoi++;
       
     }); 


 
 
   //   var ref = new Firebase("https://bacphu.firebaseio.com/");
   //   ref.onAuth(function(authData) {
   // alert(authData.uid);

// //   // if (authData.uid === null) {
  
// //   //     var url = window.location.href;
// //   //  //   var length = url.length;
// //   //     var urlorigin = url.split("#")[0];
// //   //     var length = urlorigin.length;
// //   //     var loginUrl = urlorigin.substring(0, length - 10) + "login.html";
               
// //   //             window.location.href = loginUrl;  
// //   // } else {
// //    //  var url = window.location.href;
// //    // //   var length = url.length;
// //    //    var urlorigin = url.split("#")[0];
// //    //    var length = urlorigin.length;
// //    //    var loginUrl = urlorigin.substring(0, length - 10) + "login.html";
               
// //    //            window.location.href = loginUrl;  
// //   // }
  // });

    $scope.authfacebook = AuthFacebook;

    // any time auth status updates, add the user data to scope
    $scope.authfacebook.$onAuth(function(authDataFacebook) {
      $scope.authDataFB = authDataFacebook;
     
      if ($scope.authDataFB === null)

      {

   $scope.authfacebook.$unauth();
         var url = window.location.href;
      
     
        var urlorigin = url.split("#")[0];
        var length = urlorigin.length;
        var loginUrl = urlorigin.substring(0, length - 10) + "login.html";
               
              window.location.href = loginUrl;  
            }

    });

    $scope.AdminLogout = function()
    {

       $scope.authfacebook.$unauth();
    }


  }]);


app.directive('ngForma', function() {
  return {
   
    templateUrl : 'template/AdminTemplateSanPham.html'
  };
});
 

  app.config(['$routeProvider', function($routeProvider){
      $routeProvider.when('/sanpham',
      {
        templateUrl: 'template/AdminTemplateSanPham.html',
        controller: 'AdminTemplateSanPhamController'
      }).when('/donhang',
      {
        templateUrl: 'template/AdminTemplateDonHang.html',
        controller: 'AdminTemplateDonHangController'
      }).when('/tinnhan',
      {
        templateUrl: 'template/AdminTemplateTinNhan.html',
        controller: 'AdminTemplateTinNhanController'
      }).when('/khachhang',
      {
        templateUrl: 'template/AdminTemplateKhachHang.html',
        controller: 'AdminTemplateKhachHangController'
      }).when('/chinhsuadanhmuc',
      {
        templateUrl: 'template/AdminTemplateSanPham.html',
        controller: 'AdminTemplateSanPhamController'
      }).when('/sanpham/:loaiSanPham', {
       templateUrl: 'template/AdminTemplateSanPhamTheoLoai.html',
        controller: 'AdminTemplateSanPhamTheoLoaiController'
      })

      .otherwise({
        redirectTo: '/sanpham'
      });
  }]);


 

(function($) {
    var dropdown = $('.dropdown');

    // Add slidedown animation to dropdown
    dropdown.on('show.bs.dropdown', function(e){
        $(this).find('.dropdown-menu').first().stop(true, true).slideDown();
    });

    // Add slideup animation to dropdown
    dropdown.on('hide.bs.dropdown', function(e){
        $(this).find('.dropdown-menu').first().stop(true, true).slideUp();
    });
})(jQuery);

function titleUp(string)
{
  return string.charAt(0).toUpperCase() + string.slice(1); 
}  

//login

app.factory("AuthFacebook", ["$firebaseAuth",
  function($firebaseAuth) {
    var ref = new Firebase("https://bacphu.firebaseio.com");
    return $firebaseAuth(ref);
  }
]);

 

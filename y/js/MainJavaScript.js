var app = angular.module('myApp',['ngMaterial', 'ngRoute','firebase',  'ngAnimate', 'ngTouch','angular-notification-icons']);
      app.config(function($mdThemingProvider) {
        $mdThemingProvider.theme('default')
        .primaryPalette('pink', {
          'default': '400', // by default use shade 400 from the pink palette for primary intentions
          'hue-1': '100', // use shade 100 for the <code>md-hue-1</code> class
          'hue-2': '600', // use shade 600 for the <code>md-hue-2</code> class
          'hue-3': 'A100' // use shade A100 for the <code>md-hue-3</code> class
      })
        // If you specify less than all of the keys, it will inherit from the
        // default shades
        .accentPalette('grey', {
          'default': '700' // use shade 200 for default, and keep all other shades the same
      });
    });

 
  app.controller('AppCtrl', ['$scope', '$mdSidenav', '$mdDialog',
   '$mdMedia', '$firebaseArray', '$routeParams', '$filter', "AuthFacebook",
    function($scope, $mdSidenav, $mdDialog, $mdMedia,
     $firebaseArray, $routeParams, $filter, AuthFacebook)
  {
    $scope.TenShop = "PQ Silver";
    $scope.TenShopDayDu = "PQ Silver - Bạc Phú Quý";
    $scope.DiaChiShop = "150/43 Nguyễn Trãi, phường Bến Thành, Quận 1, Thành phố Hồ Chí Minh";
    $scope.GioiThieuTrangChu = [
     {
        content: "PQSilver – chuyên cung cấp trang sức làm từ bạc (92.5 và 99.9) với cam kết đền gấp đôi giá trị đơn hàng nếu khách hàng phát hiện bất cứ sản phẩm bạc mua tại PQSilver là hàng giả."
      },
    {
        content: "SHOP BẠC PHÚ QUÝ - PQSILVER"
      },
    {
      content: "Địa chỉ Change Everyday 150/43 Nguyễn Trãi, Bến Thành, Q1" 
    },
    {
      content: "Inbox để đặt hàng" 
    },
    {
      content: "Hotline đặt hàng: 0903 176 879"
    }];
    $scope.DanhMucMenu = [
    {
      danhmuc: "Trang chủ"
    },
  {
    danhmuc: "Sản phẩm"
  },
  {
    danhmuc: "Giới thiệu"
  },
  {
    danhmuc: "Liên hệ"  
  },
  {
    danhmuc: "Đo size nhẫn"
  }
    ];
    $scope.DanhMucSanPham = [
    {
      loaisanpham: "Nhẫn"
    },
    {
  loaisanpham: "Dây chuyền"
    },
    {
   loaisanpham: "Lắc tay"
    },
    {
   loaisanpham: "Lắc chân"
    },
    {
         loaisanpham: "Hoa tai"
    }];

  $scope.ButtonMuaHang = [
  {
    TenButton: "Mua ngay",
      
  },
  {
    TenButton: "giỏ hàng",
    IconButton: "shopping_cart"
  },
  {
    TenButton: "Thêm vào giỏ hàng",
    IconButton: "shopping_cart"
  }
  ];

  $scope.DataGioHang = [
  {
  title: "Sản phẩm"
  },
  {
  title: "Giá"
  },
  {
  title: "Số lượng"
  },
  {
    title: "Thành tiền" 
  }
  ];
  $scope.DataGioHangThanhToan = [
  {
    title: "Tổng cộng" 
  },
  {
     title: "ĐẶT MUA" 
  }];
  $scope.currentVND = "VNĐ";

  //template Trang chu
  $scope.HinhAnhSlide = [
  {
    imageslide:
    "https://scontent-hkg3-1.xx.fbcdn.net/v/t1.0-9/c0.0.851.315/p851x315/13419183_930725367036846_5036183270411503920_n.jpg?oh=158a7997f4e7a2fadb14f56f9150dd33&oe=57EE580F"
  },
  {
    imageslide: 
    "https://scontent-hkg3-1.xx.fbcdn.net/v/t1.0-9/946_839713626138021_2897258258483360676_n.png?oh=e19b5b8c036b19bf68b5d4653f9d1af2&oe=57F1E1E2"
  },
  {
    imageslide: "https://scontent-hkg3-1.xx.fbcdn.net/v/t1.0-9/1972323_844243655685018_1926678498204828311_n.png?oh=4caafc77376a32ae773fcdaf06236a22&oe=5801FDAD"
  },
  {
    imageslide: "https://scontent-hkg3-1.xx.fbcdn.net/v/t1.0-9/12189024_818533448256039_7413009516864415515_n.png?oh=8f5a92f35ce74d1b0737cb716eebbbde&oe=5806F6EE"
  }];
  // Data dat mua gio hang
  $scope.DatMuaGioHang = [
  {
    title: "Thông tin khách hàng"
  },
  { 
  title: "Tên của bạn"
  },
{
  title: "Điện thoại"
},{
  title: "Địa chỉ"
}, 
{
  title: "Đặt mua" 
}];

  //data tin nhan

 $scope.DataTinNhan = {
 title: "Để lại lời nhắn",
 name: "Tên của bạn",
 phone: "Số điện thoại",
 email: "Email",
 content: "Lời nhắn",
 send: "Gửi"
  };
  
//Data xem san pham chi tiet
$scope.DataChiTietSanPham = 
{
  masanpham: "Mã sản phẩm: "
};

$scope.DataLogin = {
  name: "Tài khoản",
  password: "Mật khẩu",
  login: "Đăng nhập",
  loginfacebook: "Đăng nhập bằng facebook",
  signup: "Đăng ký",
  forgotpassword: "Quên mật khẩu"
};

$scope.DataDangKyTaiKhoan = 
{
  name: "Tên của bạn",
  user: "Tài khoản",
  password: "Mật khẩu",
  password2: "Nhập lại mật khẩu",
  gender: "Giới tính",
  birthday: "Ngày sinh",
  phone: "Số điện thoại",
  address: "Địa chỉ",
  nam: "Nam", 
  nu: "Nữ",
  title: "Đăng ký tài khoản",
  button: "Đăng ký"
};

    //mo sidenav
    $scope.motab = function(menuid)
    {
      $mdSidenav(menuid).toggle();
    }
    //mo thanh menu 
    var originatorEv;
    $scope.openMenu = function($mdOpenMenu, ev)
    {
      originatorEv  = ev;
      $mdOpenMenu(ev);
    }
 
    //mo dialog login
    $scope.status = '  ';
      $scope.customFullscreen = $mdMedia('xs') || $mdMedia('sm');
    
      $scope.showLoginDialog = function(ev) 
    {
        var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;

        $mdDialog.show(
        {
            clickOutsideToClose: true,
            controller: DialogController,
            templateUrl: 'template/MainLogin.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            
            fullscreen: useFullScreen
        })
          .then(function(answer) {
          $scope.status = 'You said the information was "' + answer + '".';
        }, function() {
         $scope.status = 'You cancelled the dialog.';
        });


        $scope.$watch(function() {
          return $mdMedia('xs') || $mdMedia('sm');
         }, function(wantsFullScreen) {
         $scope.customFullscreen = (wantsFullScreen === true);
        });
 
    };

    var refSanPham = new Firebase("https://bacphu.firebaseio.com/SanPham/");
    $scope.ListSanPham = $firebaseArray(refSanPham);

    var refSanPhamTrangChu = new Firebase("https://bacphu.firebaseio.com/SanPham/");

    
    $scope.ListSanPhamTrangChu = [];
    refSanPhamTrangChu.orderByChild("ID").limitToFirst(8).on("child_added", function(snapshot)
      {
          $scope.ListSanPhamTrangChu.push(snapshot.val());
      });

    

 
     var refSanPhamTheoLoai = new Firebase("https://bacphu.firebaseio.com/SanPham/");
  

  //xac dinh so luong page hien thi trang san pham
     var steppage = 0;
     var page = 0;
  $scope.PageArray = [];

  refSanPham.on("child_added", function(snapshot)
    {
      if (steppage === 0)
      {
        $scope.PageArray.push(page);
        page++;
      }
       if (steppage === $scope.NumberProductOnPage - 1)
      {
        steppage = -1;
      }
      steppage++;

    });
   
    
  $scope.NumberProductOnPage = 6;
  $scope.currentPage = 0;

   $scope.begin = 0;
    $scope.end = $scope.NumberProductOnPage ;
   // var refSanPhamonPage = new Firebase("https://bacphu.firebaseio.com/SanPham/");
 
 $scope.SanPhamOnPage =  [];
  //load san pham luc moi hien o che do mac dinh
   refSanPhamTrangChu.orderByChild("ID").limitToFirst($scope.NumberProductOnPage).on("child_added", function(snapshot)
      {
          $scope.SanPhamOnPage.push(snapshot.val());
      });

  $scope.changeCurrentPage = function(it)
  {
    
    $scope.currentPage = it;
   
    uploadCurrentPage();
  }
  function uploadCurrentPage()
  {
    $scope.begin = $scope.currentPage * $scope.NumberProductOnPage ;
    $scope.end = $scope.currentPage * $scope.NumberProductOnPage  + $scope.NumberProductOnPage ;
    if($scope.selectedsapxep  === 1 || $scope.selectedsapxep === 2)
    { 
      $scope.SanPhamOnPage = $scope.SanPhamSapXep.slice($scope.begin , $scope.end);
    }
    else
    {
      //luc khoi tao
       $scope.SanPhamOnPage = $scope.ListSanPham.slice($scope.begin , $scope.end);
    }
  }
  $scope.changePagePrev = function()
  {
    if ($scope.currentPage > 0)
    {
      $scope.currentPage--;
       uploadCurrentPage();
    }
  }
  $scope.changePageNext = function()
  {
    if ($scope.currentPage < $scope.PageArray.length - 1)
    {
      $scope.currentPage++;
       uploadCurrentPage();
    }
   
  }
 
$scope.selectedsapxep = "0";
$scope.listsapxep = [{id:0, name: "Mặc định"},{id:1, name: "Giá tăng dần"},
{id: 2, name: "Giá giảm dần"}];    
  $scope.changesapxep = function(loaisapxep)
  {
       $scope.currentPage = 0;
     $scope.SanPhamSapXep = [];
      $scope.selectedsapxep = loaisapxep.id;
      if ( $scope.selectedsapxep === 1)
      {
          $scope.SanPhamSapXep = $filter('orderBy')($scope.ListSanPham, 'GiaSanPham', false);
           uploadCurrentPage();
            
      }
      else if ( $scope.selectedsapxep === 2)
      {
          $scope.SanPhamSapXep = $filter('orderBy')($scope.ListSanPham, 'GiaSanPham', true);
           uploadCurrentPage();
         
      }
      else
      {
          
        $scope.SanPhamSapXep =  $filter('orderBy')($scope.ListSanPham, 'ID', false);
        uploadCurrentPage();
        
      }
     
  }  
   
   $scope.$watch('SearchSanPham', function(val)
    { 
        $scope.SanPhamSapXep = $filter('filter')($scope.ListSanPham, val);
         uploadCurrentPage();
 
    });
  
    
   $scope.SoLuongSanPhamGioHangDem = 0;
  $scope.gioHang = [];
  $scope.TongThanhToan = 0;
    $scope.SoLuongSanPham = 1;

    $scope.ThemSoLuong = function()
    {
      $scope.SoLuongSanPham++;
    }
    $scope.GiamSoLuong = function()
    {
      $scope.SoLuongSanPham--;

     
      if  ($scope.SoLuongSanPham < 1)
      {
         
        $scope.SoLuongSanPham = 1;
      }
    }
  $scope.xoaGioHang = function(item)
    {
     if (KiemTraPhanTuTonTaiTheoID($scope.gioHang, item) != -1)
      {
        var index = KiemTraPhanTuTonTaiTheoID($scope.gioHang, item);
        $scope.TongThanhToan = $scope.TongThanhToan - 
        (parseInt(ChuyenChuoiGiaMatSpace($scope.gioHang[index].GiaSanPham)) * item.SoLuongMua); 

        
        $scope.TongThanhToanSpace = ChuyenGiaSangStringSpace($scope.TongThanhToan);
         
        $scope.gioHang.splice(index, 1);
         $scope.SoLuongSanPhamGioHangDem--;
         //cap nhat thanh tien

 
      }
    }
    $scope.TimThongTinSanPham = function(sanpham)
    {
   
      $scope.laySanPham = sanpham;
      $scope.laySanPham.currentShowImage = sanpham.ImagePhu1;
    }

$scope.changeShowImage = function(image)
{
   $scope.laySanPham.currentShowImage = image;
}
   $scope.ThemSoLuongItem = function(item)
    {
      
      item.SoLuongMua++;

         item.ThanhTien = 
       (parseInt(ChuyenChuoiGiaMatSpace(item.GiaSanPham))
       * item.SoLuongMua);
       item.ThanhTienSpace = ChuyenGiaSangStringSpace(item.ThanhTien);
       
        $scope.TongThanhToan += (parseInt(ChuyenChuoiGiaMatSpace(item.GiaSanPham)));
      $scope.TongThanhToanSpace = ChuyenGiaSangStringSpace($scope.TongThanhToan);
    }

      $scope.GiamSoLuongItem = function(item)
    {
      item.SoLuongMua--;


      if  ( item.SoLuongMua < 1)
      {
         
        item.SoLuongMua = 1;

      }
      if (item.flag === false)
      {
        if (  item.SoLuongMua > 1)
        {
          item.ThanhTien = 
         (parseInt(ChuyenChuoiGiaMatSpace(item.GiaSanPham))
         * item.SoLuongMua);
         item.ThanhTienSpace = ChuyenGiaSangStringSpace(item.ThanhTien);
         
          $scope.TongThanhToan -= (parseInt(ChuyenChuoiGiaMatSpace(item.GiaSanPham)));
        $scope.TongThanhToanSpace = ChuyenGiaSangStringSpace($scope.TongThanhToan);

         }
         if (item.SoLuongMua === 1)
         {
          item.ThanhTien = 
         (parseInt(ChuyenChuoiGiaMatSpace(item.GiaSanPham))
         * item.SoLuongMua);
         item.ThanhTienSpace = ChuyenGiaSangStringSpace(item.ThanhTien);
         
          $scope.TongThanhToan -= (parseInt(ChuyenChuoiGiaMatSpace(item.GiaSanPham)));
        $scope.TongThanhToanSpace = ChuyenGiaSangStringSpace($scope.TongThanhToan);
          item.flag = true;
         }
      }
     }
    


  $scope.DatMuaHang = function(ev)
  {
       var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;

      $mdDialog.show(
        {
              clickOutsideToClose: true,
              controller: MainThemDonMuaHangController,
              templateUrl: 'template/MainTemplateThongTinDatMuaHang.html',
              parent: angular.element(document.body),
              scope: $scope,
              preserveScope: true,
              targetEvent: ev,
            
              fullscreen: useFullScreen
            })
          .then(function(answer) 
          {
            
          }, function()
          {
             $scope.status = 'You cancelled the dialog.';
          });


      $scope.$watch(function() 
      {
         return $mdMedia('xs') || $mdMedia('sm');
      }, function(wantsFullScreen) 
      {
          $scope.customFullscreen = (wantsFullScreen === true);
      });
       
  }
  

  
    //xem chi tiet san pham
    $scope.openDialogXemChiTietSanPham = function(ev)
    {

      var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;

        $mdDialog.show(
        {
            clickOutsideToClose: true,
            controller: DialogController,
            templateUrl: 'template/MainTemplateChiTietSanPham.html',
          scope: $scope,
          preserveScope: true,
            parent: angular.element(document.body),
            targetEvent: ev,
            fullscreen: useFullScreen
        })
          .then(function(answer) {
          $scope.status = 'You said the information was "' + answer + '".';
        }, function() {
         $scope.status = 'You cancelled the dialog.';
        });


        $scope.$watch(function() {
          return $mdMedia('xs') || $mdMedia('sm');
         }, function(wantsFullScreen) {
         $scope.customFullscreen = (wantsFullScreen === true);
        });
    }
  
  
  $scope.openDialogThemTinNhan = function(ev)
  {
       var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;

       $mdDialog.show(
        {
              clickOutsideToClose: true,
              controller: MainThemTinNhanController,
              scope: $scope,
                preserveScope: true,
              templateUrl: 'template/MainDialogThemTinNhan.html',
              parent: angular.element(document.body),
              targetEvent: ev,
              
              fullscreen: useFullScreen
            })
          .then(function(answer) 
          {
            
          }, function()
          {
             $scope.status = 'You cancelled the dialog.';
          });


      $scope.$watch(function() 
      {
         return $mdMedia('xs') || $mdMedia('sm');
      }, function(wantsFullScreen) 
      {
          $scope.customFullscreen = (wantsFullScreen === true);
      });
    }


   $scope.TimIDXemPhongToHinhAnh = function(sanpham)
   {
    $scope.ImageSP = sanpham;
   }
   $scope.showBigPicture = function(ev)
   {
     var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;

       $mdDialog.show(
        {
              clickOutsideToClose: true,
              controller: DialogBigImageController,
              templateUrl: 'template/MainTemplatePhongToSanPham.html',
              parent: angular.element(document.body),
              targetEvent: ev,
              scope: $scope,
          preserveScope: true,
              fullscreen: useFullScreen
            })
          .then(function(answer) 
          {
            
          }, function()
          {
             $scope.status = 'You cancelled the dialog.';
             //hien dialog xem chi tiet ssan pham lai
             $scope.openDialogXemChiTietSanPham();
              
          });


      $scope.$watch(function() 
      {
         return $mdMedia('xs') || $mdMedia('sm');
      }, function(wantsFullScreen) 
      {
          $scope.customFullscreen = (wantsFullScreen === true);
      });
   }

  //mua 1 san pham
    $scope.MuaNgaySanPhamCard = function(item)
    {
       
       if (KiemTraPhanTuTonTaiTheoID($scope.gioHang, item) === -1)
      {
        
        item.SoLuongMua = $scope.SoLuongSanPham;
         item.flag = false;
        $scope.gioHang.push(item);

      //tinh tong tien thanh toan
        item.ThanhTien = 
       (parseInt(ChuyenChuoiGiaMatSpace(item.GiaSanPham))
       * item.SoLuongMua);
       item.ThanhTienSpace = ChuyenGiaSangStringSpace(item.ThanhTien);
       
        $scope.TongThanhToan += (parseInt(ChuyenChuoiGiaMatSpace(item.GiaSanPham)) * item.SoLuongMua);
        $scope.TongThanhToanSpace = ChuyenGiaSangStringSpace($scope.TongThanhToan);
     
     $scope.SoLuongSanPhamGioHangDem++;
    //gan bien tra ve cho viec them san pham lan thu 2
      $scope.SoLuongSanPham = 1;

    }
      $mdDialog.hide();
    //chuyen den bang gio hang thanh toan
      var url = window.location.href.split("#")[0] + "#/giohang";
     window.location.href = url;  
    }
    $scope.ThemVaoGioHangCard = function(item)
    {
           var index;
  //  alert($scope.gioHang.length);

  //item khong xuat hien trong gio hang
    if (KiemTraPhanTuTonTaiTheoID($scope.gioHang, item) === -1)
      {
         
        item.SoLuongMua = $scope.SoLuongSanPham;
         item.flag = false;
        $scope.gioHang.push(item);


         item.ThanhTien = 
       (parseInt(ChuyenChuoiGiaMatSpace(item.GiaSanPham))
       * item.SoLuongMua);
       item.ThanhTienSpace = ChuyenGiaSangStringSpace(item.ThanhTien);
       
        $scope.TongThanhToan += (parseInt(ChuyenChuoiGiaMatSpace(item.GiaSanPham)) * item.SoLuongMua);
      $scope.TongThanhToanSpace = ChuyenGiaSangStringSpace($scope.TongThanhToan);

       $scope.SoLuongSanPhamGioHangDem++;
      $mdDialog.hide();

      }
      else
      {
        
         index = KiemTraPhanTuTonTaiTheoID($scope.gioHang, item);
        $scope.gioHang[index].SoLuongMua = $scope.gioHang[index].SoLuongMua + $scope.SoLuongSanPham;

         $scope.gioHang[index].ThanhTien = 
       (parseInt(ChuyenChuoiGiaMatSpace(item.GiaSanPham))
       * $scope.gioHang[index].SoLuongMua);
        $scope.gioHang[index].ThanhTienSpace = ChuyenGiaSangStringSpace($scope.gioHang[index].ThanhTien);
       
   
        $scope.TongThanhToan += (parseInt(ChuyenChuoiGiaMatSpace(item.GiaSanPham)) * $scope.SoLuongSanPham);
      $scope.TongThanhToanSpace = ChuyenGiaSangStringSpace($scope.TongThanhToan);

      }
      // co can bam nhieu lan nut them vao gio hang so luong tang???
              
      
      $mdDialog.hide();
    
    //gan bien tra ve cho viec them san pham lan thu 2
      $scope.SoLuongSanPham = 1;
    
      
    }

    //login admin
  
    $scope.GoTopPage = function()
    {
      $('html, body').animate({ scrollTop: 0 }, 'media');
    }
    //login, dang ky tai khoan
    $scope.MainOpenDialogLogin = function(ev)
   {
    
      var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;

       $mdDialog.show(
        {
              clickOutsideToClose: true,
              controller: MainLoginController,
              scope: $scope,
                preserveScope: true,
              templateUrl: 'template/MainLogin.html',
              parent: angular.element(document.body),
              targetEvent: ev,
              
              fullscreen: useFullScreen
            })
          .then(function(answer) 
          {
            
          }, function()
          {
             $scope.status = 'You cancelled the dialog.';
          });


      $scope.$watch(function() 
      {
         return $mdMedia('xs') || $mdMedia('sm');
      }, function(wantsFullScreen) 
      {
          $scope.customFullscreen = (wantsFullScreen === true);
      });
   }


    $scope.authfacebook = AuthFacebook;

    // any time auth status updates, add the user data to scope
    $scope.authfacebook.$onAuth(function(authDataFacebook) {
      $scope.authDataFB = authDataFacebook;
    });
 
 $scope.LoginFacebook = function()
 {
   $scope.authfacebook.$authWithOAuthPopup('facebook');
   $mdDialog.hide();
  alert($scope.authDataFB.facebook.accessToken);
  console.log($scope.authDataFB.facebook.accessToken);
 }
$scope.MainLogout = function()
{
   $scope.authfacebook.$unauth();
}

   var originatorEv;
     $scope.openMenu = function($mdOpenMenu, ev)
     {
      originatorEv  = ev;
      $mdOpenMenu(ev);
     }
   
  }]);
     
 

function DialogController($scope, $mdDialog) {
  $scope.hide = function() {
    $mdDialog.hide();
  };

  $scope.cancel = function() {
    $mdDialog.cancel();
  };

  $scope.answer = function(answer) {
    $mdDialog.hide(answer);
  };

  
    $scope.boVaoGioHang11 = function(item)
    {
      
         var index;
  //  alert($scope.gioHang.length);

  //item khong xuat hien trong gio hang
    if (KiemTraPhanTuTonTaiTheoID($scope.gioHang, item) === -1)
      {
         
        item.SoLuongMua = $scope.SoLuongSanPham;
        item.flag = false;
        $scope.gioHang.push(item);


         item.ThanhTien = 
       (parseInt(ChuyenChuoiGiaMatSpace(item.GiaSanPham))
       * item.SoLuongMua);
       item.ThanhTienSpace = ChuyenGiaSangStringSpace(item.ThanhTien);
       
        $scope.TongThanhToan += (parseInt(ChuyenChuoiGiaMatSpace(item.GiaSanPham)) * item.SoLuongMua);
      $scope.TongThanhToanSpace = ChuyenGiaSangStringSpace($scope.TongThanhToan);

      $scope.SoLuongSanPhamGioHangDem++;
      $mdDialog.hide();

      }
      else
      {
        
         
         index = KiemTraPhanTuTonTaiTheoID($scope.gioHang, item);
        $scope.gioHang[index].SoLuongMua = $scope.gioHang[index].SoLuongMua + $scope.SoLuongSanPham;

         $scope.gioHang[index].ThanhTien = 
       (parseInt(ChuyenChuoiGiaMatSpace(item.GiaSanPham))
       * $scope.gioHang[index].SoLuongMua);
        $scope.gioHang[index].ThanhTienSpace = ChuyenGiaSangStringSpace($scope.gioHang[index].ThanhTien);
       
   
        $scope.TongThanhToan += (parseInt(ChuyenChuoiGiaMatSpace(item.GiaSanPham)) * $scope.SoLuongSanPham);
      $scope.TongThanhToanSpace = ChuyenGiaSangStringSpace($scope.TongThanhToan);
      }
      // co can bam nhieu lan nut them vao gio hang so luong tang???
              
      
      $mdDialog.hide();
    
    //gan bien tra ve cho viec them san pham lan thu 2
      $scope.SoLuongSanPham = 1;
       
      
    }

    $scope.MuaNgaySanPhamChiTiet = function(item)
    {

      if (KiemTraPhanTuTonTaiTheoID($scope.gioHang, item) === -1)
      {
         
        item.SoLuongMua = $scope.SoLuongSanPham;
         item.flag = false;
        $scope.gioHang.push(item);

      //tinh tong tien thanh toan
        item.ThanhTien = 
       (parseInt(ChuyenChuoiGiaMatSpace(item.GiaSanPham))
       * item.SoLuongMua);
       item.ThanhTienSpace = ChuyenGiaSangStringSpace(item.ThanhTien);
       
        $scope.TongThanhToan += (parseInt(ChuyenChuoiGiaMatSpace(item.GiaSanPham)) * item.SoLuongMua);
        $scope.TongThanhToanSpace = ChuyenGiaSangStringSpace($scope.TongThanhToan);
     
     $scope.SoLuongSanPhamGioHangDem++;
    //gan bien tra ve cho viec them san pham lan thu 2
      $scope.SoLuongSanPham = 1;

    }
      $mdDialog.hide();
    //chuyen den bang gio hang thanh toan
      var url = window.location.href.split("#")[0] + "#/giohang";
     window.location.href = url;  
    }
  
 

}
 
function ChuyenChuoiGiaMatSpace(giasp)
{
  var number = "";
  var arrayNumber = giasp.split(" ");
  for (var i = 0; i < arrayNumber.length; i++)
  {
     
     number = number +  arrayNumber[i];
  }
 
  return number;

}
function ChuyenGiaSangStringSpace(price)
{
  //chuoi dao nguoc.
  var chuoiSoDinhDang = "";
  var chuoiSoTraVe = "";
  var chuoisoString = price.toString();
  var i;
  var step = 0; 
  for (i = chuoisoString.length - 1; i >= 0; i--)
  {
    if (step < 3)
    {
      chuoiSoDinhDang += chuoisoString[i];
      step++;
    }
    else
    {
      chuoiSoDinhDang += " " + chuoisoString[i];  
      step = 0;
    }
  }
  var j;
  for (j = chuoiSoDinhDang.length - 1; j >= 0; j--)
  {
    chuoiSoTraVe += chuoiSoDinhDang[j];
  }
  return chuoiSoTraVe;


}
function KiemTraPhanTuTonTaiTheoID(array, item)
{
  var length = array.length;
  
  for (var i = 0; i < length; i++)
  {
    if (array[i].ID === item.ID)
    {
      return i;
    }
  }
  return -1;
}


//foute URL link
app .config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/trangchu', {
        templateUrl: 'template/MainTrangChu.html',
        controller: 'MainGioiThieuController'
      }).when('/sanpham',{
        templateUrl: 'template/MainSanPham.html',
        controller: 'MainGioiThieuController'
      }).
      when('/gioithieu', {
        templateUrl: 'template/MainGioiThieu.html',
        controller: 'MainGioiThieuController'
      }).when('/lienhe',{
        templateUrl: 'template/MainLienHe.html',
        controller: 'MainGioiThieuController'
      }).when('/dosizenhan',{
        templateUrl: 'template/MainDoSizeNhan.html',
        controller: 'MainGioiThieuController'
      }).when('/giohang',{
        templateUrl: 'template/MainTemplateXemGioHang.html',
        controller: 'MainGioiThieuController'
      }).
      when('/sanpham/:loaiSanPham',{
        templateUrl: 'template/MainSanPhamTheoLoai.html',
        controller: 'MainSanPhamTheoLoaiController'
      }). 
      otherwise({
        redirectTo: '/trangchu'
      });
  }]);
 app.controller('MainGioiThieuController', function($scope)
 {
  $scope.message = "Hello";
 });

//hinh anh noi bat
app.controller('ProductController', function($products, $scope) {
    this.filterBy         = "Lượt xem"; 
    this.availableFilters = $products.availableFilters;       
})
.factory('$products',function(){
  return {
    availableFilters : ["Lượt xem", "Giá tăng dần", "Giá giảm dần"]     
  };
})

 
app.controller('slideShowController', function($scope, $timeout) {
 var slidesInSlideshow = 4;
 var slidesTimeIntervalInMs = 3500; 

 $scope.slideshow = 1;
 var slideTimer =
 $timeout(function interval() {
     $scope.slideshow = ($scope.slideshow % slidesInSlideshow) + 1;
     slideTimer = $timeout(interval, slidesTimeIntervalInMs);
     }, slidesTimeIntervalInMs);
 });


 //factory auth dang ky tai khoan, login
  app.factory('authFactory', ['$firebaseAuth', function( $firebaseAuth) {
    var authFactory = {};
    var ref = new Firebase("https://bacphu.firebaseio.com/");
    
    var auth = $firebaseAuth(ref);
    console.log(auth);
    authFactory.createUser = function(email, password) {
        return auth.$createUser({
            email: email,
            password: password
        })};

     // Authentication 
     authFactory.authUser = function(email, password) {
      return auth.$authWithPassword({
            email: email,
            password: password
        });
     }
    return authFactory;
}]);

//login facebook


app.factory("AuthFacebook", ["$firebaseAuth",
  function($firebaseAuth) {
    var ref = new Firebase("https://bacphu.firebaseio.com");
    return $firebaseAuth(ref);
  }
]);




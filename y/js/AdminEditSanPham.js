

app.controller('AdminEditSanPhamController', 
  ['$scope',   '$mdDialog', '$mdMedia', '$mdToast', '$firebaseArray', '$firebaseObject',  
  function($scope,   $mdDialog, $mdMedia, $mdToast, $firebaseArray, $firebaseObject)
{

	$scope.curSP =[];
	$scope.curSP.TenSP = $scope.CurrentSanPhamEdit.TenSanPham;
	$scope.curSP.GiaSP = $scope.CurrentSanPhamEdit.GiaSanPham;
	$scope.curSP.LoaiSP = $scope.CurrentSanPhamEdit.LoaiSanPham;
	$scope.curSP.SoLuongSP = $scope.CurrentSanPhamEdit.SoLuongSanPham;
	$scope.curSP.MoTaSP = $scope.CurrentSanPhamEdit.MoTaSanPham;

	
	  var refKey = $scope.CurrentSanPhamEdit.$id;
  
	var ref = new Firebase("https://bacphu.firebaseio.com/SanPham/"+ refKey);
  		 		 
	$scope.EditSanPham = [];
	$firebaseObject(ref).$bindTo($scope, "EditSanPham");

	$scope.SaveEditSanPham = function()
	{
	  
    	 $scope.EditSanPham.TenSanPham  = $scope.curSP.TenSP;
    	   $scope.EditSanPham.GiaSanPham = $scope.curSP.GiaSP;
    	  $scope.EditSanPham.LoaiSanPham = $scope.curSP.LoaiSP;
    	  $scope.EditSanPham.MoTaSanPham = $scope.curSP.MoTaSP;
    	  $scope.EditSanPham.SoLuongSanPham  = parseInt($scope.curSP.SoLuongSP);
 		
 		 $mdToast.show(
         $mdToast.simple().textContent('Chinh sua san pham thanh cong!')
         .position('top right')
         .hideDelay(5000)

        );
    	$mdDialog.cancel();
	}

}]);

 

app.controller('AdminEditSanPhamTheoLoaiController', 
  ['$scope',   '$mdDialog', '$mdMedia', '$mdToast', '$firebaseArray', '$firebaseObject',  
  function($scope,   $mdDialog, $mdMedia, $mdToast, $firebaseArray, $firebaseObject)
{

	$scope.curSPTheoLoai =[];
	$scope.curSPTheoLoai.TenSP = $scope.CurrentSanPhamEditTheoLoai.TenSanPham;
	$scope.curSPTheoLoai.GiaSP = $scope.CurrentSanPhamEditTheoLoai.GiaSanPham;
	$scope.curSPTheoLoai.LoaiSP = $scope.CurrentSanPhamEditTheoLoai.LoaiSanPham;
	$scope.curSPTheoLoai.SoLuongSP = $scope.CurrentSanPhamEditTheoLoai.SoLuongSanPham;
	$scope.curSPTheoLoai.MoTaSP = $scope.CurrentSanPhamEditTheoLoai.MoTaSanPham;
	 
 
	// lay link url san pham theo loai
 	
	  var refKeyTheoLoai = $scope.CurrentSanPhamEditTheoLoai.$id;
  
  
	var refTheoLoai = new Firebase("https://bacphu.firebaseio.com/SanPham/"+ refKeyTheoLoai);
  		 		 
	$scope.EditSanPhamTheoLoai = [];
	$firebaseObject(refTheoLoai).$bindTo($scope, "EditSanPhamTheoLoai");
 
	$scope.SaveEditSanPhamTheoLoai = function()
	{
	  

  	 	 $scope.EditSanPhamTheoLoai.TenSanPham  = $scope.curSPTheoLoai.TenSP;
    	   $scope.EditSanPhamTheoLoai.GiaSanPham = $scope.curSPTheoLoai.GiaSP;
    	  $scope.EditSanPhamTheoLoai.LoaiSanPham = $scope.curSPTheoLoai.LoaiSP;
    	  $scope.EditSanPhamTheoLoai.MoTaSanPham = $scope.curSPTheoLoai.MoTaSP;
    	  $scope.EditSanPhamTheoLoai.SoLuongSanPham  = parseInt($scope.curSPTheoLoai.SoLuongSP);

  

 		//hien thi thong bao thanh cong
 		 $mdToast.show(
         $mdToast.simple().textContent('Chinh sua san pham thanh cong!')
         .position('top right')
         .hideDelay(5000)

        );
    	$mdDialog.cancel();
	}


}]);

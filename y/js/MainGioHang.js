function MainThemDonMuaHangController($scope, $mdDialog, $firebaseArray, $mdToast)
{
		$scope.hide = function() {
		$mdDialog.hide();
	  };

	  $scope.cancel = function() {
		$mdDialog.cancel();
	  };

	  $scope.answer = function(answer) {
		$mdDialog.hide(answer);
	  };
	  
  
	 $scope.ThemDonMuaHang = function()
	{
    	var tenMuaHang = $scope.NameMuaHang;
			var dienThoaiMuaHang = $scope.PhoneMuaHang;
			var diaChiMuaHang = $scope.AddressMuaHang;
			 var currentDate = new Date()
			var day = currentDate.getDate();
			var month = currentDate.getMonth() + 1;
			var year = currentDate.getFullYear();
			var curDate = day + "/" + month + "/" + year;
			
			 var refDonHang = new Firebase("https://bacphu.firebaseio.com/DonHang");
       $scope.ListDonHang = $firebaseArray(refDonHang);
         
      $scope.ListDonHang.$loaded().then(function(ListDonHang) {
        var ListDonHangLength = ListDonHang.length; 
         var newIDDonHang;
        if (ListDonHangLength === 0)
        {
          newIDDonHang = 0;
        }
        else
        {
          newIDDonHang = parseInt(ListDonHang[ListDonHangLength - 1].IDDonHang) + 1;
        }
        var newDonHang = refDonHang.push();
           newDonHang.set(
            {
              TenMuaHang : tenMuaHang,
              DienThoaiMuaHang : dienThoaiMuaHang,
              DiaChiMuaHang: diaChiMuaHang,
              NgayMuaHang: curDate,
              IDDonHang : newIDDonHang,
              SoLuongSanPham : parseInt($scope.gioHang.length),
              TinhTrangDaGiao: false
            },
            function(error) {
        if (error) {
            $mdToast.show(
         $mdToast.simple().textContent('Đặt mua hàng thất bại')
         .position('bottom right')
         .hideDelay(5000)

        );
        } else {
        $mdToast.show(
         $mdToast.simple().textContent('Bạn đã đặt mua hàng thành công')
         .position('bottom right')
         .hideDelay(5000)

        );
         }
        });
 
      // //them vao chi tiet don hang
    
       var refChiTietDonHang = new Firebase("https://bacphu.firebaseio.com/ChiTietDonHang");
    
          for (var i = 0; i < $scope.gioHang.length; i++)
          {
            var newCTDonHang = refChiTietDonHang.push();
            newCTDonHang.set({
              IDDonHang : newIDDonHang,
              IDSanPham : $scope.gioHang[i].ID, 
              SoLuong : parseInt($scope.gioHang[i].SoLuongMua)
            });
           
          }       
          //mua gio hang xong
         $scope.gioHang  = [];
        $scope.TongThanhToan = 0;
        $scope.TongThanhToanSpace = ChuyenGiaSangStringSpace($scope.TongThanhToan);
       $scope.SoLuongSanPhamGioHangDem = 0;

      });
 
		$mdDialog.cancel();
	}

}

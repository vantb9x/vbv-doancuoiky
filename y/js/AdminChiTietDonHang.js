app.controller('ChiTietDonHangController',function($scope, $firebaseArray, $mdDialog, $mdMedia)
{
	var refChiTietDonHang = new Firebase("https://bacphu.firebaseio.com/ChiTietDonHang");
	$scope.ListChiTietDonHang = $firebaseArray(refChiTietDonHang);

	var refSanPham = new Firebase("https://bacphu.firebaseio.com/SanPham");
	$scope.ListSanPhamDonHang = $firebaseArray(refSanPham);

	 $scope.ListCTTheoID = [];
 	
 	$scope.TongThanhToanTatCa = 0;
 	$scope.ListSanPhamDonHang.$loaded().then(function(ListSanPhamDonHang)
 	{
 		$scope.ListChiTietDonHang.$loaded().then(function(ListChiTietDonHang)
		{
			var leng = ListChiTietDonHang.length;

			for (var i = 0; i < leng; i++)
			{
				if (ListChiTietDonHang[i].IDDonHang === $scope.CurrentIDDonHang)
				{
					var TenSanPham;
					//alert(ListChiTietDonHang[i].IDSanPham);

					 var thongtinSanPham = [];
					refSanPham.orderByChild("ID").equalTo(ListChiTietDonHang[i].IDSanPham).on("child_added", function(snapshot)
					{		
						thongtinSanPham.TenSanPham = snapshot.val().TenSanPham;
						thongtinSanPham.GiaSanPham = snapshot.val().GiaSanPham;
						thongtinSanPham.MaSanPham = snapshot.val().MaSanPham;
						thongtinSanPham.TongTienSanPham =  
						(parseInt(ChuyenChuoiGiaMatSpace(thongtinSanPham.GiaSanPham))
     									  * ListChiTietDonHang[i].SoLuong);
    			  		thongtinSanPham.ThanhTienSpace = ChuyenGiaSangStringSpace(thongtinSanPham.TongTienSanPham);


					});
						 
					
					 
					 var ChiTietDonHang = [];
					 ChiTietDonHang = ListChiTietDonHang[i];
					 ChiTietDonHang.Name = thongtinSanPham.TenSanPham;
					 ChiTietDonHang.GiaSanPham = thongtinSanPham.GiaSanPham; 
					 ChiTietDonHang.ThanhTienSpace = thongtinSanPham.ThanhTienSpace;
					 ChiTietDonHang.MaSanPham = thongtinSanPham.MaSanPham;
					$scope.TongThanhToanTatCa += 
					(parseInt(ChuyenChuoiGiaMatSpace( ChiTietDonHang.GiaSanPham)) * ListChiTietDonHang[i].SoLuong);
     				$scope.TongThanhToanTatCaSpace = ChuyenGiaSangStringSpace($scope.TongThanhToanTatCa);
					$scope.ListCTTheoID.push(ChiTietDonHang);
		 			 
				}
			}
		});
 			
 	});
  


	 
});


function ChuyenChuoiGiaMatSpace(giasp)
{
  var number = "";
  var arrayNumber = giasp.split(" ");
  for (var i = 0; i < arrayNumber.length; i++)
  {
     
     number = number +  arrayNumber[i];
  }
 
  return number;

}
function ChuyenGiaSangStringSpace(price)
{
  //chuoi dao nguoc.
  var chuoiSoDinhDang = "";
  var chuoiSoTraVe = "";
  var chuoisoString = price.toString();
  var i;
  var step = 0; 
  for (i = chuoisoString.length - 1; i >= 0; i--)
  {
    if (step < 3)
    {
      chuoiSoDinhDang += chuoisoString[i];
      step++;
    }
    else
    {
      chuoiSoDinhDang += " " + chuoisoString[i];  
      step = 0;
    }
  }
  var j;
  for (j = chuoiSoDinhDang.length - 1; j >= 0; j--)
  {
    chuoiSoTraVe += chuoiSoDinhDang[j];
  }
  return chuoiSoTraVe;


}
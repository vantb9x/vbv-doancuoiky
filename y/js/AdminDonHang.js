 
app.controller('AdminTemplateDonHangController', 
  ['$scope',   '$mdDialog', '$mdMedia', '$mdToast', '$firebaseArray', '$firebaseObject', '$filter',  
  function($scope,   $mdDialog, $mdMedia, $mdToast, $firebaseArray, $firebaseObject, $filter)
{
	var refDonHang = new Firebase("https://bacphu.firebaseio.com/DonHang");
	
	$scope.DonHang = $firebaseArray(refDonHang);
 	
 
	$scope.TimIDDonHang = function(item)
	  {
	  	var index = KiemTraPhanTuTonTaiTheoIDDonHang($scope.DonHang, item);
	  	//dung cho ham xem chi tiet don hang
	  	$scope.CurrentIDDonHang = $scope.DonHang[index].IDDonHang;


	  	//chinh sua don hang
	  	$scope.CurrentDonHang = $scope.DonHang[index];
	  
	  
	  }
	 

 	 $scope.xemChiTietDonHang = function(ev)
	  {
	  	 var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;

	     $mdDialog.show(
	   		{
			      	clickOutsideToClose: true,
			        controller: DialogEditSanPhamController,
			        templateUrl: 'template/AdminTemplateXemChiTietDonHang.html',
			       	scope: $scope,
					preserveScope: true,

			        parent: angular.element(document.body),
			        targetEvent: ev,
			        
			        fullscreen: useFullScreen
	      		})
	     		.then(function(answer) 
	     		{
    			  if (answer === "OK")
    			  {
    			  	$scope.ListSanPham.$remove(item);
    			  }
    			}, function()
    			{
     				 $scope.status = 'You cancelled the dialog.';
   		 		});


  		$scope.$watch(function() 
  		{
     		 return $mdMedia('xs') || $mdMedia('sm');
    	}, function(wantsFullScreen) 
    	{
      		$scope.customFullscreen = (wantsFullScreen === true);
   		});
	  }

	  //cap nhat don hang
	
 
	  $scope.CheckTinhTrangDaGiao = function()
	  {
 		  var refkeycurDH = $scope.CurrentDonHang.$id;
	  
	  	  var refURLDonHang = new Firebase("https://bacphu.firebaseio.com/DonHang/"+ 	refkeycurDH);
	  	  
	  
 	 	   var curDonHangCheck = $scope.CurrentDonHang;
	  	    var tinhtrang =  $scope.CurrentDonHang.TinhTrangDaGiao;
	  	
	  	refURLDonHang.update({
			  "TinhTrangDaGiao": tinhtrang
			});
	  	 
 				 
 		 //cap nhat so luong san pham


 	var refChiTietDonHang = new Firebase("https://bacphu.firebaseio.com/ChiTietDonHang");
	$scope.ListChiTietDonHang = $firebaseArray(refChiTietDonHang);

	var refSanPham = new Firebase("https://bacphu.firebaseio.com/SanPham");
	$scope.ListSanPhamDonHang = $firebaseArray(refSanPham);

	$scope.ListCTTheoID = [];
 
 	$scope.ListSanPhamDonHang.$loaded().then(function(ListSanPhamDonHang)
 	{
 		$scope.ListChiTietDonHang.$loaded().then(function(ListChiTietDonHang)
		{
			var leng = ListChiTietDonHang.length;

			for (var i = 0; i < leng; i++)
			{
				if (ListChiTietDonHang[i].IDDonHang === $scope.CurrentIDDonHang)
				{
					var SoLuongBan;
					//alert(ListChiTietDonHang[i].IDSanPham);

					 
					refSanPham.orderByChild("ID").equalTo(ListChiTietDonHang[i].IDSanPham).on("child_added", function(snapshot)
					{		
						SoLuongBan = snapshot.val().SoLuongBan;
						var keysanpham = snapshot.key();
						var soluongmua = ListChiTietDonHang[i].SoLuong;

						var soluongcapnhat = SoLuongBan + soluongmua;
						//cap nhat so luong o day
						var SanPhamUpdateSoLuongMua = refSanPham.child(keysanpham);
						SanPhamUpdateSoLuongMua.update({
			  				"SoLuongBan": soluongcapnhat
						});
					});
						 
			 
		 			 
				}
			}
		});
 			
 	});
   
	  }

		$scope.flagDonHangTinhTrang = false;
 	$scope.SapXepDonHangTheoTinhTrang = function()
 	{
 		if ($scope.flagDonHangTinhTrang === true)
 		{
 		$scope.DonHang = $filter('orderBy')($scope.DonHang, 'TinhTrangDaGiao', true);
 		$scope.flagDonHangTinhTrang = false;
 		 }
 		 else
 		 {
 		 		$scope.DonHang = $filter('orderBy')($scope.DonHang, 'TinhTrangDaGiao', false);
 		 		$scope.flagDonHangTinhTrang  = true;
 		 }
 	}

 		$scope.flagDonHangID = false;
 	$scope.SapXepDonHangTheoID = function()
 	{
 		if ($scope.flagDonHangID === true)
 		{
 		$scope.DonHang = $filter('orderBy')($scope.DonHang, 'IDDonHang', true);
 		$scope.flagDonHangID = false;
 		 }
 		 else
 		 {
 		 		$scope.DonHang = $filter('orderBy')($scope.DonHang, 'IDDonHang', false);
 		 		$scope.flagDonHangID  = true;
 		 }
 	}

 		$scope.flagDonHangNgayMua = false;
 	$scope.SapXepDonHangTheoNgayMua = function()
 	{
 		if ($scope.flagDonHangNgayMua === true)
 		{
 		$scope.DonHang = $filter('orderBy')($scope.DonHang, 'NgayMuaHang', true);
 		$scope.flagDonHangNgayMua = false;
 		 }
 		 else
 		 {
 		 		$scope.DonHang = $filter('orderBy')($scope.DonHang, 'NgayMuaHang', false);
 		 		$scope.flagDonHangNgayMua  = true;
 		 }
 	}
 		$scope.flagDonHangKhachHang = false;
 	$scope.SapXepDonHangTheoKhachHang = function()
 	{
 		if ($scope.flagDonHangKhachHang === true)
 		{
 		$scope.DonHang = $filter('orderBy')($scope.DonHang, 'TenMuaHang', true);
 		$scope.flagDonHangKhachHang = false;
 		 }
 		 else
 		 {
 		 		$scope.DonHang = $filter('orderBy')($scope.DonHang, 'TenMuaHang', false);
 		 		$scope.flagDonHangKhachHang  = true;
 		 }
 	}

	 
}]);


function DialogEditSanPhamController($scope, $mdDialog) {
  $scope.hide = function() {
    $mdDialog.hide();
  };
  $scope.cancel = function() {
    $mdDialog.cancel();
  };
  $scope.answer = function(answer) {
    $mdDialog.hide(answer);
  };
}

function KiemTraPhanTuTonTaiTheoIDDonHang(array, item)
{
  var length = array.length;
  
  for (var i = 0; i < length; i++)
  {
    if (array[i].IDDonHang === item.IDDonHang)
    {
      return i;
    }
  }
  return -1;
}